# Car Finder 1.0

Car finder is a tool that I wrote to be able to quickly lookg for cars on my favourite Car sites.

## Install

```text
docker pull cbaxter1988/car_finder
```

## Testing

To run all unit test use the following command.

```text
docker run cbaxter1988/car_finder pytest /tests 
```

## Basic Usage

```bash
docker run --rm cbaxter1988/car_finder cf-truecar-cli -make bmw -model 5-series -year_range 2017-2020
```

## Current Coverage Report

```text
Name                                                       Stmts   Miss  Cover   Missing
----------------------------------------------------------------------------------------
car_finder\api\run.py                                          5      5     0%   1-9
car_finder\configuration\constants.py                         12      0   100%
car_finder\data_access\dao.py                                 14      0   100%
car_finder\data_access\truecar_table_dao.py                  125      0   100%
car_finder\models\data_models\common_car_data_model.py        13      0   100%
car_finder\models\data_models\truecar_data_model.py           10      0   100%
car_finder\models\model.py                                     3      0   100%
car_finder\models\table_models\car_finder_table_model.py      20      0   100%
car_finder\models\table_models\table_model.py                  3      0   100%
car_finder\scrappers\truecar_scrapper.py                      78      2    97%   165, 181
car_finder\utils\aws_utils.py                                 10      2    80%   26-28
car_finder\utils\bs4_utils.py                                  9      0   100%
car_finder\utils\car_stat_utils.py                            60      0   100%
car_finder\utils\flask_api_utils.py                           21      0   100%
car_finder\utils\http_utils.py                                 3      0   100%
car_finder\utils\iter_utils.py                                 5      0   100%
car_finder\utils\log_utils.py                                 18      0   100%
car_finder\utils\model_utils.py                               10      0   100%
car_finder\utils\regex_utils.py                                3      0   100%
car_finder\utils\test_utils.py                                 6      0   100%
car_finder\utils\truecar_utils.py                             18      0   100%
car_finder\utils\url_utils.py                                  9      0   100%
----------------------------------------------------------------------------------------
TOTAL                                                        455      9    98%


```