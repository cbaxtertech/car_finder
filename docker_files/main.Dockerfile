FROM python

COPY requirements.txt /_src/
COPY dist/car_finder-1.tar.gz /_src/
COPY tests /tests

ENV MONGO_DB_HOST=192.168.1.5
ENV MONGO_DB_PORT=27017
ENV MONGO_APP_DB=car_finder_db
ENV EXECUTION_TABLE_ID=execution_table_default
ENV VEHICLE_TABLE_ID=vehicle_table_default
ENV INVALID_VIN_TABLE_ID=invalid_vin_table_default
ENV SCRAPPER_EXCHANGE=cf_exchange
ENV SCRAPPER_QUEUE=cf_queue
ENV SCRAPPER_ROUTING_KEY=scrapper

RUN pip install -r /_src/requirements.txt
RUN pip install /_src/car_finder-1.tar.gz
RUN rm -rf _src