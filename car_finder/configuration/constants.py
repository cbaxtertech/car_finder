import os

from car_finder.utils.env_utils import get_env_strict, get_env
from pconst import Const


class ConfigConstants(Const):
    STAGE = get_env_strict('STAGE')

    AWS_ACCESS_KEY_ID = get_env("AWS_ACCESS_KEY_ID")
    AWS_SECRET_ACCESS_KEY_ID = get_env("AWS_SECRET_ACCESS_KEY_ID")
    AWS_DEFAULT_REGION = get_env("AWS_DEFAULT_REGION", "us-east-1")

    MONGO_DB_HOST = get_env_strict(key='MONGO_DB_HOST', raise_exception=True)
    MONGO_DB_PORT = get_env_strict(key='MONGO_DB_PORT', raise_exception=True)
    MONGO_APP_DB = get_env_strict(key="MONGO_APP_DB", raise_exception=True)

    VEHICLE_TABLE_ID = get_env_strict(key="VEHICLE_TABLE_ID")
    EXECUTION_TABLE_ID = get_env_strict(key="EXECUTION_TABLE_ID")
    INVALID_VIN_TABLE_ID = get_env_strict(key="INVALID_VIN_TABLE_ID")

    API_BIND_ADDRESS = get_env("API_BIND_ADDRESS", default_value="0.0.0.0")
    API_BIND_PORT = get_env("API_BIND_PORT", default_value="5001")
    API_DEBUG = get_env("API_DEBUG", False)

    AMQP_USER = get_env_strict("AMQP_USER", raise_exception=True)
    AMQP_PW = get_env_strict("AMQP_PW")
    AMQP_HOST = get_env_strict("AMQP_HOST")
    AMQP_PORT = get_env_strict("AMQP_PORT")
    AMQP_URL = get_env("AMQP_URL", default_value=f'amqp://{AMQP_USER}:{AMQP_PW}@{AMQP_HOST}:{AMQP_PORT}')

    SCRAPPER_EXCHANGE = os.getenv("SCRAPPER_EXCHANGE")
    SCRAPPER_QUEUE = os.getenv("SCRAPPER_QUEUE")
    SCRAPPER_ROUTING_KEY = os.getenv("SCRAPPER_ROUTING_KEY")

    UPDATER_QUEUE = os.getenv("UPDATER_QUEUE")
    UPDATER_EXCHANGE = os.getenv("UPDATER_EXCHANGE")
    UPDATER_ROUTING_KEY = os.getenv("UPDATER_ROUTING_KEY")

    FILTER_WORKER_QUEUE = os.getenv("FILTER_WORKER_QUEUE")
    FILTER_WORKER_EXCHANGE = os.getenv("FILTER_WORKER_EXCHANGE")
    FILTER_WORKER_ROUTING_KEY = os.getenv("FILTER_WORKER_ROUTING_KEY")


def get_const():
    return ConfigConstants()
