from enum import Enum

from car_finder.utils.cloudwatch_utils import get_cloudwatch_client, update_metric, create_dimension

NAME_SPACE = 'CarFinderMetricsTest'


class Metrics(Enum):
    APIScrapperRequest: str = 'APIScrapperRequest'
    InvalidDataSet: str = 'InvalidDataSet'
    ValidDataSet: str = 'ValidDataSet'
    APIDepreciationRequest: str = 'APIDepreciationRequest'


class MetricManager:

    def __init__(self):
        self._client = get_cloudwatch_client()

    def _update_metric(self, metric_name, dimensions, value):
        update_metric(
            client=self._client,
            name_space=NAME_SPACE,
            metric_name=metric_name,
            dimensions=dimensions,
            value=value

        )

    def increment_api_scrapper_request_metric(self, make, model, source):
        _metric_name = Metrics('APIScrapperRequest').value

        self._update_metric(
            metric_name=_metric_name,
            dimensions=[
                create_dimension(
                    name=f'{_metric_name}<Make - Model>',
                    value=f'{make} - {model}'
                ),
                create_dimension(
                    name=f'{_metric_name}<Source>',
                    value=f'{source}'
                )
            ],
            value=1
        )

    def increment_invalid_dataset_metric(self, make, model, source):
        _metric_name = Metrics('InvalidDataSet').value

        self._update_metric(
            metric_name=_metric_name,
            dimensions=[
                create_dimension(
                    name=f'{_metric_name}<Make, Model>',
                    value=f'{make} - {model}'
                ),
                create_dimension(
                    name=f'{_metric_name}<Source>',
                    value=f'{source}'
                )
            ],
            value=1
        )

    def increment_valid_dataset_metric(self, make, model, vin):
        _metric_name = Metrics('ValidDataSet').value

        self._update_metric(
            metric_name=_metric_name,
            dimensions=[
                create_dimension(
                    name=f'{_metric_name}(Make, Model, vin)',
                    value=f'{make} - {model} - {vin}'
                )
            ],
            value=1
        )

    def increment_api_depreciation_request_metric(self, make, model, trim):
        _metric_name = Metrics('APIDepreciationRequest').value
        self._update_metric(
            metric_name=_metric_name,
            dimensions=[
                create_dimension(
                    name=f'{_metric_name}_Make',
                    value=f'{make}'
                ),
                create_dimension(
                    name=f'{_metric_name}_Model',
                    value=f'{model}'
                ),
                create_dimension(
                    name=f'{_metric_name}_Trim',
                    value=f'{trim}'
                ),

            ],
            value=1
        )
