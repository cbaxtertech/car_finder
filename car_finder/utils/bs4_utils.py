from bs4 import BeautifulSoup
from bs4.element import Tag
from car_finder.utils.http_utils import get_html_page


def extract_result_set_by_attr(tag: Tag, attrs):
    return tag.findChildren(attrs=attrs)
    # return tag.findChildren(attrs=attrs)


def get_parsed_html_page(url) -> BeautifulSoup:
    html_data = get_html_page(url)
    parsed_paged = BeautifulSoup(html_data, 'html.parser')
    return parsed_paged
