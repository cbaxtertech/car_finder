import re


def make_pattern(pattern, flags):
    return re.compile(pattern=pattern, flags=flags)
