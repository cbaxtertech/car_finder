from statistics import mean
from typing import List, Tuple, NamedTuple

from car_finder.models.data_models.vehicle_data_model import VehicleDataModel
from car_finder.utils.log_utils import get_global_logger

logger = get_global_logger()


class DepreciationData(NamedTuple):
    year: int
    percent_change: str
    price_diff: int
    price: int
    percent_int: float


class AveragePriceReturnVal(NamedTuple):
    price: int
    price_list: List[int]


class DepreciationReport(NamedTuple):
    start_year: int
    start_price: int
    data: List[DepreciationData]


def extract_year_and_price_from_model(models: List[VehicleDataModel]) -> List[Tuple[int, int]]:
    """
    Iterates through a list of models and creates a tuple containing the Year and Price of the car.

    :param models:
    :return:
    """
    return [(int(model.year), model.price_num)
            for model in models]


def calculate_avg_price(car_records: List[VehicleDataModel]) -> AveragePriceReturnVal:
    """
    Takes a list for TrueCarDataModels and calculates the average cost.

    :param car_records:
    :return:
    """

    logger.debug("Calculating Average Cost")
    _price_list = [car.price_num for car in car_records]
    average = mean(_price_list)
    return AveragePriceReturnVal(price=average, price_list=_price_list)


def calculate_depreciation_from_year(
        start_year: int,
        year_price_tuples: List[Tuple[int, int]],

) -> DepreciationReport:
    """
    Calculates the percent change between year_prices from the start year.

    :param start_year: Year to start calculation from
    :param year_price_tuples: list of year and price data to be used for calculation. IE [(2019, 50000)]
    :return List[DepreciationReturnVal]:
    """
    return _calculate_depreciation(start_year=start_year, year_price_tuples=year_price_tuples)


def calculate_model_depreciation_from_year(start_year: int, models: List[VehicleDataModel]):
    year_price_tuples = extract_year_and_price_from_model(models=models)

    num_agg_map = {}
    for year, price in year_price_tuples:
        num_agg_map[year] = []

    for year, price in year_price_tuples:
        if price > 0:
            num_agg_map[year].append(price)

    results = []

    for year in num_agg_map.keys():
        results.append((year, mean(num_agg_map[year])))

    results = sorted(results, key=lambda a: a[0], reverse=True)

    return _calculate_depreciation(start_year=start_year, year_price_tuples=results)


def _calculate_depreciation(start_year: int, year_price_tuples: List[Tuple[int, int]]):
    """
    Calculates the depreciation for data in year_price_tuples based starting from  start_year.

    :param year_price_tuples:
    :param start_year:
    :return:
    """
    _start_year, _start_price = _get_start_data(prices=year_price_tuples, start_year=start_year)

    results = []
    for year, price in year_price_tuples:
        if year < _start_year:
            change, percent_int, price_diff = _calculate_percent_change(new=price, old=_start_price)
            results.append(
                DepreciationData(
                    year=year,
                    percent_change=change,
                    price_diff=price_diff,
                    percent_int=percent_int,
                    price=price
                )
            )

    return DepreciationReport(start_year=_start_year, start_price=_start_price, data=results)


def _calculate_percent_change(new, old):
    _price_dif = round((new - old))
    _percent_int = round((_price_dif / old))
    _percent = _format_percent((_price_dif / old))
    return _percent, _percent_int * 100, _price_dif


def _format_percent(percent):
    return "{:.2%}".format(percent)


def _get_start_data(prices, start_year):
    for _year, _price in prices:
        if _year == start_year:
            return _year, _price
