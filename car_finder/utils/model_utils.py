import json
from dataclasses import dataclass, asdict, fields


def model_to_dict(model: dataclass) -> dict:
    return asdict(model)


def model_to_json(model: dataclass) -> str:
    return json.dumps(model_to_dict(model=model))


def list_fields(model: dataclass) -> list:
    _fields = fields(model)
    _fields = [f.name for f in _fields]
    return _fields
