import re


def extract_vin_from_url(url: str):
    if "truecar.com" in url:
        pattern = r"listing\/(\w*)\/"
        compiled_pattern = re.compile(pattern)
        matches = compiled_pattern.findall(url)
        if len(matches) == 1:
            return matches.pop()
        else:
            return url
