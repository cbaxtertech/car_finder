import json
from http import HTTPStatus
from typing import Any, List, Iterable

from car_finder.models.model import Model
from car_finder.utils.model_utils import model_to_json, model_to_dict
from flask import Response

JSON_RESPONSE_HEADERS = {'content-type': 'application/json; charset=utf-8'}


def _make_json_data_response(data: Any, headers=None, status: int = HTTPStatus.OK):
    if not headers:
        headers = JSON_RESPONSE_HEADERS

    return Response(headers=headers, response=json.dumps({"data": data}), status=status)


def _make_json_items_response(items: Iterable, headers=None, status: int = HTTPStatus.OK):
    if not headers:
        headers = JSON_RESPONSE_HEADERS

    if not isinstance(items, Iterable):
        raise TypeError("Must be an iterable ")

    return Response(headers=headers, response=json.dumps({"items": items}), status=status)


def make_data_response(
        type: str,
        data: Any,
        headers: dict = None,
        status: int = HTTPStatus.OK
):
    if type == "json":
        return _make_json_data_response(data=data, headers=headers, status=status)


def make_response_from_items(
        type: str,
        items: Iterable,
        headers: dict = None,
        status: int = HTTPStatus.OK
):
    if type == "json":
        return _make_json_items_response(items, headers=headers, status=status)


def make_api_response_from_model(
        type: str,
        model: Model,
        headers: dict = None,
        status: int = HTTPStatus.OK
):
    _data = model_to_json(model=model)

    if type == "json":
        return _make_json_data_response(data=_data, headers=headers, status=status)


def make_response_from_models(
        type: str,
        models: List[Model],
        headers: dict = None,
        status: int = HTTPStatus.OK
):
    _data = [
        model_to_dict(model=model)
        for model in models
    ]

    if type == "json":
        return _make_json_data_response(data={"records": _data, "count": len(_data)}, headers=headers, status=status)
