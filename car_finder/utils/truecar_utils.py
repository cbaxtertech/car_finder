import re
from typing import List

from car_finder.models.data_models.truecar_data_model import TrueCarDataModel

from car_finder.utils.iter_utils import filter_list


def extract_records_wo_accidents(records: List[TrueCarDataModel]):
    def _filter_no_accident(record: TrueCarDataModel):
        pattern = re.compile(r"No accidents")
        results = pattern.findall(record.car_condition)
        if results:
            return True

    return filter_list(_filter_no_accident, records)


def extract_records_w_accidents(records: List[TrueCarDataModel]):
    def _filter_no_accident(record: TrueCarDataModel):
        pattern = re.compile(r"[1-9] accidents")
        results = pattern.findall(record.car_condition)
        if results:
            return True

    return filter_list(_filter_no_accident, records)