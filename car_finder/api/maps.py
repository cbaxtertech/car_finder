from car_finder.data_access.vehicle_table_dao import VehicleTableDAO
from car_finder.scrappers.truecar_scrapper import TruecarScrapper

default_mapping = {
    "dao_class": None,
    "scrapper_class": None
}

SOURCE_MAP = {
    "default": default_mapping,
    "truecar": {
        "dao_class": VehicleTableDAO,
        "scrapper_class": TruecarScrapper,
    }
}
