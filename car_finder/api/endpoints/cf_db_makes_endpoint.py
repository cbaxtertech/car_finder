from http import HTTPStatus

from car_finder.data_access.vehicle_table_dao import VehicleTableDAO
from car_finder.utils.flask_api_utils import make_response_from_items
from flask_restful import Resource, reqparse

parser = reqparse.RequestParser()

parser.add_argument("store_results", type=str)


class CarFinderDBModelsResource(Resource):

    @staticmethod
    def get(make):

        dao = VehicleTableDAO()

        results = dao.list_make_models(make=make)
        if not results:
            results = []
        else:
            results = list(results)
            results.sort()

        return make_response_from_items(type="json", items=results, status=HTTPStatus.OK)


class CarFinderDBMakesResource(Resource):

    @staticmethod
    def get():

        dao = VehicleTableDAO()

        results = dao.list_makes()
        if not results:
            results = []
        else:
            results = list(results)
            results.sort()

        return make_response_from_items(type="json", items=results, status=HTTPStatus.OK)


class CarFinderDBYearsByMakeModelResource(Resource):

    @staticmethod
    def get(make, model):

        dao = VehicleTableDAO()

        results = dao.list_years_by_make_model(make=make, model=model)
        if not results:
            results = []
        else:
            results = list(results)
            results.sort()

        return make_response_from_items(type="json", items=results, status=HTTPStatus.OK)


class CarFinderDBYearsByMakeModelTrimResource(Resource):

    @staticmethod
    def get(make, model, trim):
        dao = VehicleTableDAO()

        results = dao.list_years_by_make_model_trim(make=make, model=model, trim=trim)
        if not results:
            results = []
        else:
            results = list(results)
            results.sort()

        return make_response_from_items(type="json", items=results, status=HTTPStatus.OK)