from http import HTTPStatus
from car_finder.data_access.vehicle_table_dao import VehicleTableDAO
from car_finder.utils.flask_api_utils import make_response_from_items
from flask_restful import Resource, reqparse

parser = reqparse.RequestParser()

parser.add_argument("store_results", type=str)


class CarFinderDBTrimsResource(Resource):

    @staticmethod
    def get(make, model):

        dao = VehicleTableDAO()

        results = dao.list_model_trims(model=model, make=make)
        if not results:
            results = []
        else:
            results = list(results)

            results = [res for res in results if not '\xa0' in res]

            results.sort()

        return make_response_from_items(type="json", items=results, status=HTTPStatus.OK)
