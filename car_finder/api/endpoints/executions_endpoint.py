from http import HTTPStatus

from car_finder.data_access.execution_dao import ExecutionDAO
from car_finder.utils.flask_api_utils import make_data_response
from car_finder.utils.log_utils import get_logger
from car_finder.utils.model_utils import model_to_dict
from flask_restful import Resource

logger = get_logger(name='executions_endpoint')


class ExecutionResource(Resource):

    def get(self, execution_id):
        dao = ExecutionDAO()
        logger.info(f"Fetching Execution: {execution_id}")
        model = dao.get_execution(execution_id=execution_id)
        if model:
            return make_data_response("json", data=model_to_dict(model), status=HTTPStatus.OK)
        else:
            return make_data_response('json', data={"message": "Not Found"}, status=HTTPStatus.NOT_FOUND)


class PendingExecutionsResource(Resource):
    @staticmethod
    def _serialize_models(models):
        _models = []
        for model in models:
            del model.context
            _models.append(
                {
                    "execution_id": model._id,
                    "request": model.request,
                    "created": model.created,
                    "utc_start_time": model.utc_start_time,
                    "utc_end_time": model.utc_end_time,
                }
            )

        return _models


    def get(self):
        payload = {}
        dao = ExecutionDAO()
        payload['pending_filtering'] = self._serialize_models(dao.list_executions_pending())
        payload['filtering'] = self._serialize_models(dao.list_executions_filtering())
        payload['in_queue'] = self._serialize_models(dao.list_executions_queued())
        payload['scrapping'] = self._serialize_models(dao.list_executions_scrapping())
        payload['in_progress'] = self._serialize_models(dao.list_executions_in_progress())
        payload['saving_context'] = self._serialize_models(dao.list_executions_saving_context())
        payload['failed'] = self._serialize_models(dao.list_executions_failed())

        return make_data_response("json", data=payload, status=HTTPStatus.OK)


class CompletedExecutionsResource(Resource):
    @staticmethod
    def _serialize_models(models):
        _models = []
        for model in models:

            _models.append(
                {
                    "execution_id": model._id,
                    "request": model.request,
                    "finding_count": len(model.context)
                }
            )

        return _models


    def get(self):
        payload = {}
        dao = ExecutionDAO()
        payload['completed'] = self._serialize_models(dao.list_executions_completed())


        return make_data_response("json", data=payload, status=HTTPStatus.OK)