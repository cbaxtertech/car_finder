from http import HTTPStatus

from bson import ObjectId
from car_finder.data_access.vehicle_table_dao import VehicleTableDAO
from car_finder.utils.flask_api_utils import make_data_response
from flask import request
from flask_restful import Resource


class CFDBQueryResource(Resource):
    @staticmethod
    def _serialize_id(record):
        record['_id'] = str(record['_id'])
        return record

    @staticmethod
    def post():
        query = request.get_json()
        url_params = request.args
        limit_per_page = url_params.get("limit", 100)
        last_item_id = url_params.get("last_item_id")
        all_pages_flag = bool(url_params.get("all_pages", False))

        dao = VehicleTableDAO()
        if last_item_id:
            last_item_id = ObjectId(last_item_id)

        if not all_pages_flag:
            page = dao.get_page(query=query, limit_per_page=int(limit_per_page), last_item_id=last_item_id)

            return make_data_response(
                data={
                    "last_item_id": str(page.last_id),
                    "previous_item_id": str(last_item_id),
                    "total_items": page.total_items,
                    "items": [
                        CFDBQueryResource._serialize_id(record)
                        for record in page
                    ]
                },
                type='json',
                status=HTTPStatus.OK
            )
        else:
            paginator = dao.get_pages(query=query, limit_per_page=int(limit_per_page))
            pages = {}
            for i, page in enumerate(paginator.pages):
                i = i + 1
                for item in page.cursor:
                    CFDBQueryResource._serialize_id(item)
                    if i not in pages.keys():
                        pages[i] = []
                        pages[i] = [item]
                    else:
                        pages[i].append(item)

            return make_data_response(
                data={
                    "page_count": len(list(pages.keys())),
                    "items_per_pages": limit_per_page,
                    "pages": pages
                },
                type='json',
                status=HTTPStatus.OK
            )
