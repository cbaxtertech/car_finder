from http import HTTPStatus

from car_finder.data_access.vehicle_table_dao import VehicleTableDAO
from car_finder.metrics.metric_manager import MetricManager
from car_finder.utils.vehicle_stat_utils import calculate_model_depreciation_from_year
from car_finder.utils.flask_api_utils import make_data_response
from car_finder.utils.log_utils import get_logger
from flask import request
from flask_restful import Resource, reqparse

parser = reqparse.RequestParser()

parser.add_argument("store_results", type=str)

logger = get_logger(name="car_stat_depreciation")

metrics_manager = MetricManager()


def _generate_year_list(start_year, _range):
    return [start_year - (year + 1) for year in range(_range)]


def extract_request_params(request_body):
    make = request_body.get("make")
    model = request_body.get("model")
    trim = request_body.get("trim")
    range_int = int(request_body.get("range"))
    start_year = int(request_body.get("start_year"))

    return make, model, trim, range_int, start_year


def _add_start_year_to_year_list(year_list, start_year):
    year_list.insert(0, start_year)
    return year_list


def _get_records(year_list, make, model, trim):
    dao = VehicleTableDAO()

    records = []
    for year in year_list:

        query = {
            "year": str(year),
            "model": model,
            "make": make
        }
        if trim:
            query['trim'] = trim

        logger.debug(f"Built query='{query}'")

        _records = dao.query(query=query)
        if _records:
            records += _records

    return records


def _generate_report(data):
    return {
        "start_year": data.start_year,
        "start_average_price": data.start_price,
        "report": [
            {
                "year": d.year,
                "percent_change": d.percent_change,
                "average_price": d.price

            }
            for d in data.data
        ]
    }


class CarStatDepreciationResource(Resource):

    @staticmethod
    def post():

        request_body = request.get_json()

        make, model, trim, _range, start_year = extract_request_params(request_body=request_body)

        year_list = _generate_year_list(start_year=start_year, _range=_range)

        year_list = _add_start_year_to_year_list(year_list=year_list, start_year=start_year)

        records = _get_records(year_list=year_list, make=make, model=model, trim=trim)

        try:
            data = calculate_model_depreciation_from_year(start_year=start_year, models=records)

        except TypeError:
            logger.debug(f"No Records found for start_year: {start_year}")
            return make_data_response(
                type="json",
                data={
                    f"message": f"Invalid Request, No Records found {start_year} {make}_{model}_{trim}"
                },
                status=HTTPStatus.NOT_FOUND
            )

        reports = _generate_report(data=data)
        metrics_manager.increment_api_depreciation_request_metric(
            make=make,
            model=model,
            trim=trim
        )

        return make_data_response(type="json", data=reports, status=HTTPStatus.OK)
