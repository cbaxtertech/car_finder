from http import HTTPStatus

from car_finder.configuration.constants import get_const
from car_finder.data_access.execution_dao import ExecutionDAO, build_execution_model
from car_finder.metrics.metric_manager import MetricManager
from car_finder.utils.amqp_utils import make_basic_pika_publisher
from car_finder.utils.flask_api_utils import make_data_response
from car_finder.utils.log_utils import get_logger
from car_finder.utils.model_utils import model_to_json
from car_finder.utils.pika_utils import close_connection
from flask import request
from flask_restful import Resource

metric_manager = MetricManager()
const = get_const()
logger = get_logger("scrapper_endpoint")

STAGE = const.STAGE

SCRAPPER_EXCHANGE = f'{const.SCRAPPER_EXCHANGE}_{const.STAGE}'
SCRAPPER_ROUTING_KEY = f'{const.SCRAPPER_ROUTING_KEY}_{const.STAGE}'
SCRAPPER_QUEUE = f'{const.SCRAPPER_QUEUE}_{const.STAGE}'


def _build_execution(execution_dao, source, data):
    logger.debug(f"Building execution model for (source={source}, data={data})")
    execution_model = build_execution_model(request={"source": source, "params": data}, status='CREATED')
    execution_dao.save_execution(execution_model)
    return execution_model


def _queue_message(execution_dao, execution_model):
    logger.info(f"Queuing execution for (execution_id={execution_model._id})")
    producer = make_basic_pika_publisher(
        amqp_url=const.AMQP_URL,
        exchange=SCRAPPER_EXCHANGE,
        routing_key=SCRAPPER_ROUTING_KEY,
        queue=SCRAPPER_QUEUE
    )

    producer.publish_message(body=model_to_json(execution_model).encode())

    execution_dao.set_execution_queued(execution_id=execution_model._id)
    metric_manager.increment_api_scrapper_request_metric(
        make=execution_model.request.get("params").get("make"),
        model=execution_model.request.get("params").get("model"),
        source=execution_model.request.get("source")
    )

    close_connection(connection=producer.connection)


class ScrapperResource(Resource):

    @staticmethod
    def post(source):

        data = request.get_json()

        try:
            execution_dao = ExecutionDAO()

            execution_model = _build_execution(execution_dao=execution_dao, source=source, data=data)

            _queue_message(execution_dao=execution_dao, execution_model=execution_model)

            return make_data_response(type='json', data={"execution_id": execution_model._id}, status=HTTPStatus.OK)

        except TypeError as err:
            return make_data_response(type="json", data={"message": str(err)}, status=HTTPStatus.BAD_REQUEST)
