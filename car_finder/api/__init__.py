from car_finder.api.maps import SOURCE_MAP, default_mapping
from car_finder.api.endpoints.car_stat_depreciation_endpoint import CarStatDepreciationResource
from car_finder.api.endpoints.cf_db_makes_endpoint import CarFinderDBModelsResource, CarFinderDBMakesResource, CarFinderDBYearsByMakeModelResource, CarFinderDBYearsByMakeModelTrimResource
from car_finder.api.endpoints.cf_db_trims_endpoint import CarFinderDBTrimsResource
from car_finder.api.endpoints.scrapper_endpoint import ScrapperResource
from car_finder.api.endpoints.executions_endpoint import ExecutionResource, PendingExecutionsResource, CompletedExecutionsResource
from car_finder.api.endpoints.query_endpoint import CFDBQueryResource
from car_finder.data_access.vehicle_table_dao import VehicleTableDAO
from car_finder.utils.flask_api_utils import make_response_from_models, make_data_response
from flask import Flask, request
from flask_restful import Api, reqparse
from flask_cors import CORS, cross_origin

app = Flask(__name__)
api = Api(app)

cors = CORS(app)

request_parser = reqparse.RequestParser()


@app.route("/api/v1/cf_db/<source>/count")
def get_car_count_by_source(source):
    source_mapping = SOURCE_MAP.get(source, default_mapping)
    dao = source_mapping['dao_class']()
    count = dao.get_record_count()

    return make_data_response(data={"count": count}, type="json")

#
# @app.route("/api/v1/cf_db/query", methods=['POST'])
# def query_source():
#     dao = VehicleTableDAO()
#     _models = []
#     params = request.args
#     return_limit = int(params.get("limit", 100))
#
#     query_request = request.json
#     results = dao.query(query=query_request)
#     _models = results

    return make_response_from_models(models=_models[:return_limit], type="json")

@app.route("/api/v1/cf_db/query/paginate", methods=['POST'])
def query_source():
    dao = VehicleTableDAO()
    _models = []
    params = request.args
    return_limit = int(params.get("limit", 100))

    query_request = request.json
    results = dao.query(query=query_request)
    _models = results

    return make_response_from_models(models=_models[:return_limit], type="json")

api.add_resource(ScrapperResource, "/api/v1/scrapper/<source>")
api.add_resource(CarFinderDBTrimsResource, "/api/v1/cf_db/trims/<make>/<model>")
api.add_resource(CarFinderDBModelsResource, "/api/v1/cf_db/models/<make>")
api.add_resource(CarFinderDBMakesResource, "/api/v1/cf_db/makes")
api.add_resource(CarFinderDBYearsByMakeModelResource, "/api/v1/cf_db/years/<make>/<model>")
api.add_resource(CarFinderDBYearsByMakeModelTrimResource, "/api/v1/cf_db/years/<make>/<model>/<trim>")
api.add_resource(CarStatDepreciationResource, "/api/v1/stats/depreciation")
api.add_resource(ExecutionResource, "/api/v1/execution/<execution_id>")
api.add_resource(PendingExecutionsResource, "/api/v1/executions/pending")
api.add_resource(CompletedExecutionsResource, "/api/v1/executions/completed")
api.add_resource(CFDBQueryResource, "/api/v1/cf_db/query")
