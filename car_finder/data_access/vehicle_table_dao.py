from typing import List, Set

from car_finder.configuration.constants import get_const
from car_finder.data_access.mongodb_dao import MongoDBDAO
from car_finder.models.data_models.truecar_data_model import TrueCarDataModel, VehicleDataModel
from car_finder.models.pagination_models import NewPage
from car_finder.models.table_models.vehicle_table_model import VehicleTableModel, build_vehicle_table_model
from car_finder.utils.iter_utils import map_list
from car_finder.utils.log_utils import get_logger
from car_finder.utils.model_utils import model_to_dict
from car_finder.utils.pagination_utils import Paginator
from car_finder.utils.pymongo_utils import get_page_from_collection, get_pages_from_collection

logger = get_logger(__name__)

CONSTANTS = get_const()

mongo_db_host = CONSTANTS.MONGO_DB_HOST
mongo_db_port = int(CONSTANTS.MONGO_DB_PORT)
cf_app_db = CONSTANTS.MONGO_APP_DB
table_id = f'{CONSTANTS.VEHICLE_TABLE_ID}_{CONSTANTS.STAGE}'


class VehicleTableDAO(MongoDBDAO):

    def __init__(self, db_host=mongo_db_host, db_port=mongo_db_port, db=cf_app_db, collection=table_id):
        super().__init__(db_host, db_port, db, collection)

    def _process_query(self, query: dict):
        results = self.query(query=query)
        if results:
            return results
        else:
            return []

    def get_record_by_id(self, record_id: str) -> VehicleTableModel:
        query = _build_mongo_query_v1(_id=record_id)

        results = self.query(query=query)
        if len(results) == 1:
            return results.pop()

    def get_record_by_vin(self, vin: str) -> VehicleTableModel:
        query = _build_mongo_query_v1(vin=vin)
        results = self.query(query=query)
        if len(results) == 1:
            return results.pop()

    def get_records(self) -> List[VehicleTableModel]:
        return self.query(query={})

    def get_record_count(self, query_filter=None) -> int:
        if not query_filter:
            query_filter = {}

        return self.collection.count_documents(query_filter)

    def update_record(self, record_id, model: TrueCarDataModel) -> bool:
        newvalues = {"$set": model_to_dict(model)}

        result = self.collection.update_one(
            {
                "_id": record_id
            },
            newvalues
        )

        if result.modified_count == 1:
            return True
        else:
            return False

    def update_record_w_vin(self, vin, model: VehicleDataModel):
        newvalues = {"$set": model_to_dict(model)}

        result = self.collection.update_one(
            {
                "vin": vin

            },
            newvalues
        )

        if result.modified_count == 1:
            return True
        else:
            return False

    def save_record(self, data_model: VehicleDataModel) -> bool:
        return self.save_record_from_data_model(model=data_model)

    def get_records_by_make(self, make: str) -> List[VehicleTableModel]:
        return self._process_query(query=_build_mongo_query_v1(make=make))

    def get_records_by_make_model(self, make: str, model: str) -> List[VehicleTableModel]:
        return self._process_query(query=_build_mongo_query_v1(make=make, model=model))

    def get_records_by_make_model_trim(self, make: str, model: str, trim: str) -> List[VehicleTableModel]:
        return self._process_query(query=_build_mongo_query_v1(make=make, model=model, trim=trim))

    def get_records_by_make_model_trim_year(
            self,
            make: str,
            model: str,
            trim: str,
            year: str
    ) -> List[VehicleTableModel]:

        return self._process_query(
            query=_build_mongo_query_v1(make=make, model=model, trim=trim, year=year))

    def list_make_models(self, make: str) -> Set[str]:
        """
        Returns a list of Car models based on make.


        :param make:
        :return:
        """
        model_set = set()
        query = _build_mongo_query_v1(make=make)

        results = self._process_query(query=query)

        for result in results:
            model_set.add(result.model)

        if model_set:
            return model_set

    def list_model_trims(self, make: str, model: str) -> Set[str]:
        """
        Returns a list of model trims based on make and model.


        :param make:
        :param model:
        :return:
        """
        trim_set = set()
        query = _build_mongo_query_v1(make=make, model=model)

        results = self._process_query(query=query)

        for result in results:
            trim_set.add(result.trim)

        if trim_set:
            return trim_set

    def list_makes(self) -> List[str]:
        """
        returns a list of car makes.

        :return:
        """
        return self.collection.distinct("make")

    def list_years_by_make_model(self, make, model) -> List[str]:
        """
        returns a list of car makes.

        :return:
        """
        return self.collection.distinct("year", filter={'make': make, "model": model})

    def list_years_by_make_model_trim(self, make, model, trim) -> List[str]:
        """
        returns a list of car makes.

        :return:
        """
        return self.collection.distinct("year", filter={'make': make, "model": model, "trim": trim})

    @staticmethod
    def build_query(query: dict) -> dict:
        if query.get("year_range"):
            del query['year_range']
        return _build_mongo_query_v1(**query)

    def save_record_from_data_model(self, model: VehicleDataModel):
        _data = model_to_dict(model)

        if not self.is_vin_present(_data.get("vin")):

            response = self.collection.insert_one(
                _data
            )
            logger.info(f"Successfully Saved: {response.inserted_id} Vin: {_data.get('vin')}")
            return True

        else:
            logger.warn(
                f"Duplicate Vin, Unable to Save "
                f"'make={_data.get('make')} model={_data.get('model')} vin={_data.get('vin')}'")

            return False

    def delete_record(self, record_id: str) -> bool:
        results = self.collection.delete_one({"_id": record_id})
        logger.debug(f"Deleted '{results.deleted_count}' records")

        if results.deleted_count == 1:
            return True
        else:
            return False

    def find_all(self, make=None, model=None, year_range=None, trim=None):
        return self.query(
            query=_build_mongo_query_v1(make=make, model=model, year_range=year_range, trim=trim)
        )

    def is_vin_present(self, vin: str) -> bool:
        if self.collection.count({'vin': vin}) > 0:
            return True
        else:
            return False

    def query(self, query: dict) -> List[VehicleTableModel]:
        def _map_truecar_model(data):
            return build_vehicle_table_model(**data)

        results = self.query_items(query)
        return map_list(_map_truecar_model, results)




# Helper Functions


def _build_mongo_query_v1(
        make=None,
        model=None,
        year_range=None,
        trim=None,
        _id=None,
        year=None,
        **kwargs
) -> dict:
    query = {**kwargs}
    if make:
        query['make'] = make

    if model:
        query['model'] = model

    if year_range:
        query['year_range'] = year_range

    if year:
        query['year'] = year

    if trim:
        query['trim'] = trim

    if _id:
        query['_id'] = _id

    return query
