from typing import Tuple, Any

from car_finder.configuration.constants import get_const
from car_finder.data_access.mongodb_dao import MongoDBDAO
from car_finder.models.table_models.invalid_vin_model import InvalidVinModel
from car_finder.utils.log_utils import get_logger
from car_finder.utils.model_utils import model_to_dict
from pymongo.errors import DuplicateKeyError

logger = get_logger(__name__)
const = get_const()

mongo_db_host = const.MONGO_DB_HOST
mongo_db_port = int(const.MONGO_DB_PORT)
cf_app_db = const.MONGO_APP_DB
table_id = f'{const.INVALID_VIN_TABLE_ID}_{const.STAGE}'


class InvalidVinDAO(MongoDBDAO):

    def __init__(self, db_host=mongo_db_host, db_port=mongo_db_port, db=cf_app_db, collection=table_id):
        super().__init__(db_host, db_port, db, collection)

    def save_record(self, table_model: InvalidVinModel) -> Tuple[bool, Any]:

        try:
            self.add_item(item=model_to_dict(table_model))


        except DuplicateKeyError:

            logger.error(

                f"DuplicateKeyError Encountered, Unable to Save 'make={table_model.make} model={table_model.model} vin={table_model._id}'")

            return (False, None)
