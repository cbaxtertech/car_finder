from typing import List

from car_finder.configuration.constants import get_const
from car_finder.data_access.mongodb_dao import MongoDBDAO
from car_finder.decorators import log_invocation
from car_finder.models.table_models.execution_model import ExecutionModel, build_execution_model, ExecutionStatues
from car_finder.utils.log_utils import get_logger
from car_finder.utils.model_utils import model_to_dict
from car_finder.utils.time_utils import get_utc_timestamp

CONSTANTS = get_const()

mongo_db_host = CONSTANTS.MONGO_DB_HOST
mongo_db_port = int(CONSTANTS.MONGO_DB_PORT)
cf_app_db = CONSTANTS.MONGO_APP_DB
table_id = f'{CONSTANTS.EXECUTION_TABLE_ID}_{CONSTANTS.STAGE}'

logger = get_logger(__name__)


class ExecutionDAO(MongoDBDAO):

    def __init__(self, db_host=mongo_db_host, db_port=mongo_db_port, db=cf_app_db, table_id=table_id):
        super().__init__(db_host, db_port, db, table_id)

    def get_execution(self, execution_id) -> ExecutionModel:
        _record_data = self.get_item(item_id=execution_id)
        return ExecutionModel(**_record_data.next())

    def list_executions(self) -> List[ExecutionModel]:
        return [ExecutionModel(**record) for record in self.scan_items()]

    def list_executions_in_progress(self):
        return [ExecutionModel(**record) for record in self.query_items(query={"status": "IN_PROGRESS"})]

    def list_executions_scrapping(self):
        return [ExecutionModel(**record) for record in self.query_items(query={"status": "SCRAPPING"})]

    def list_executions_completed(self):
        return [ExecutionModel(**record) for record in self.query_items(query={"status": "COMPLETED"})]

    def list_executions_queued(self):
        return [ExecutionModel(**record) for record in self.query_items(query={"status": "IN_QUEUE"})]

    def list_executions_started(self):
        return [ExecutionModel(**record) for record in self.query_items(query={"status": "STARTED"})]

    def list_executions_failed(self):
        return [ExecutionModel(**record) for record in self.query_items(query={"status": "FAILED"})]

    def list_executions_pending(self):
        return [ExecutionModel(**record) for record in self.query_items(query={"status": "PENDING_FILTERING"})]

    def list_executions_filtering(self):
        return [ExecutionModel(**record) for record in self.query_items(query={"status": "FILTERING"})]

    def list_executions_saving_context(self):
        return [ExecutionModel(**record) for record in self.query_items(query={"status": "SAVING_CONTEXT"})]

    @log_invocation
    def save_execution(self, execution_model: ExecutionModel):
        execution_model.created = get_utc_timestamp()
        return self.add_item(item=model_to_dict(execution_model))

    @log_invocation
    def delete_execution(self, execution_id: str):
        return self.delete_item(item_id=execution_id)

    def _set_execution_status(self, execution_id, status):
        # execution = self.get_item(item_id=execution_id)
        #
        # _execution = build_execution_model(**execution.next())
        new_vals = {}
        if status == 'SCRAPPING':
            # _execution.utc_start_time = get_utc_timestamp()

            new_vals['utc_start_time'] = get_utc_timestamp()
        if status == 'COMPLETED':
            # _execution.utc_end_time = get_utc_timestamp()
            new_vals['utc_end_time'] = get_utc_timestamp()

        # _execution.status = ExecutionStatues(status).value
        new_vals['status'] = ExecutionStatues(status).value
        self.update_item(item_id=execution_id, new_values=new_vals)

    @log_invocation
    def set_execution_started(self, execution_id):
        self._set_execution_status(execution_id=execution_id, status="STARTED")

    @log_invocation
    def set_execution_complete(self, execution_id):
        self._set_execution_status(execution_id=execution_id, status="COMPLETED")

    @log_invocation
    def set_execution_failed(self, execution_id):
        self._set_execution_status(execution_id=execution_id, status="FAILED")

    @log_invocation
    def set_execution_pending(self, execution_id):
        self._set_execution_status(execution_id=execution_id, status="PENDING_FILTERING")

    @log_invocation
    def set_execution_saving_context(self, execution_id):
        self._set_execution_status(execution_id=execution_id, status="SAVING_CONTEXT")

    @log_invocation
    def set_execution_in_progress(self, execution_id):
        self._set_execution_status(execution_id=execution_id, status="IN_PROGRESS")

    @log_invocation
    def set_execution_status_filtering(self, execution_id):
        self._set_execution_status(execution_id=execution_id, status="FILTERING")

    @log_invocation
    def set_execution_status_pending_update(self, execution_id):
        self._set_execution_status(execution_id=execution_id, status="PENDING_UPDATE")

    @log_invocation
    def set_execution_queued(self, execution_id):
        self._set_execution_status(execution_id=execution_id, status="IN_QUEUE")

    @log_invocation
    def set_execution_status_scrapping(self, execution_id):
        self._set_execution_status(execution_id=execution_id, status="SCRAPPING")

    def set_execution_context(self, execution_id, context_data):
        logger.info(f"Saving Context for Execution: {execution_id}")
        context = context_data
        # cursor = self.get_item(item_id=execution_id)
        # execution = cursor.next()
        # execution['context'] = context
        # _execution = build_execution_model(**execution)
        # self.update_item(item_id=execution_id, new_values=model_to_dict(_execution))
        self.update_item(item_id=execution_id, new_values={"context": context_data})
