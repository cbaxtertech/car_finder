import json

from car_finder.configuration.constants import get_const
from car_finder.data_access.execution_dao import ExecutionDAO
from car_finder.data_access.vehicle_table_dao import VehicleTableDAO
from car_finder.utils.amqp_utils import make_basic_pika_consumer, make_basic_pika_publisher
from car_finder.utils.log_utils import get_logger
from car_finder.workers.worker import Worker
from pika.adapters.blocking_connection import BlockingChannel
from pika.spec import Basic, BasicProperties

const = get_const()

BROKER_URL = const.AMQP_URL
STAGE = const.STAGE
WORKER_QUEUE = const.SCRAPPER_QUEUE
WORKER_EXCHANGE = const.SCRAPPER_EXCHANGE
UPDATER_QUEUE = const.UPDATER_QUEUE
UPDATER_EXCHANGE = const.UPDATER_EXCHANGE
UPDATER_ROUTING_KEY = const.UPDATER_ROUTING_KEY
logger = get_logger(__name__)

WORKER_QUEUE = f'{WORKER_QUEUE}_{STAGE}'
WORKER_EXCHANGE = f'{WORKER_EXCHANGE}_{STAGE}'

UPDATER_QUEUE = f'{UPDATER_QUEUE}_{STAGE}'
UPDATER_EXCHANGE = f'{UPDATER_EXCHANGE}_{STAGE}'
UPDATER_ROUTING_KEY = f'{UPDATER_ROUTING_KEY}_{STAGE}'

_FILTER_WORKER_QUEUE = const.FILTER_WORKER_QUEUE
_FILTER_WORKER_EXCHANGE = const.FILTER_WORKER_EXCHANGE
_FILTER_WORKER_ROUTING_KEY = const.FILTER_WORKER_ROUTING_KEY

FILTER_QUEUE = f'{_FILTER_WORKER_QUEUE}_{STAGE}'
FILTER_EXCHANGE = f'{_FILTER_WORKER_EXCHANGE}_{STAGE}'
FILTER_ROUTING_KEY = f'{_FILTER_WORKER_ROUTING_KEY}_{STAGE}'


def on_message_callback(ch: BlockingChannel, method: Basic.Deliver, properties: BasicProperties, body):
    ch.basic_ack(delivery_tag=method.delivery_tag)


    execution_dao = ExecutionDAO()
    data = json.loads(body)
    execution_id = data.get("execution_id")
    logger.info(f"Acknowledged Message: '{method.delivery_tag}' for execution '{execution_id}'")
    execution_dao.set_execution_status_filtering(execution_id)
    logger.info(f" Processing Execution: {execution_id}")

    try:
        vehicle_table_dao = VehicleTableDAO()

        execution = execution_dao.get_execution(execution_id=execution_id)
        execution_context = execution.context
        execution_request = execution.request
        execution_request_params = execution_request.get("params")
        logger.info(f"Filtering Records for execution: {execution_id} - {execution_request_params}")
        new_records = []
        for record in execution_context:
            record: dict
            vin = record.get("vin")
            if not vehicle_table_dao.is_vin_present(vin=vin):
                logger.info(f"New Vin={vin} detected for Execution ID: {execution_id} - {execution_request_params} ")
                new_records.append(record)

        logger.info(f'Filtered {len(new_records)} new records, updating execution context')
        execution_dao.set_execution_context(execution_id=execution_id, context_data=new_records)
        updater_publisher = make_basic_pika_publisher(
            queue=UPDATER_QUEUE,
            amqp_url=BROKER_URL,
            exchange=UPDATER_EXCHANGE,
            routing_key=UPDATER_ROUTING_KEY
        )
        updater_publisher.publish_message(body={
            "execution_id": execution_id,
        })
        execution_dao.set_execution_status_pending_update(execution_id=execution_id)

        execution_dao.client.close()
        vehicle_table_dao.client.close()

    except Exception as err:
        execution_dao.set_execution_failed(execution_id=execution_id)
        execution_dao.set_execution_context(
            execution_id=execution_id,
            context_data={
                "error": str(err),
                "worker": str(__name__)
            }
        )
        logger.info(f"Execution: '{execution_id}' Failed: {err}")
        raise


class FilterWorker(Worker):

    def __init__(self):
        self._consumer = make_basic_pika_consumer(
            amqp_url=BROKER_URL,
            queue=FILTER_QUEUE,
            on_message_callback=on_message_callback
        )

    def run(self, *args, **kwargs):
        self._consumer.run()
