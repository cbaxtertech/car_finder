import json

from car_finder.configuration.constants import get_const
from car_finder.data_access.execution_dao import ExecutionDAO
from car_finder.data_access.vehicle_table_dao import VehicleTableDAO, VehicleDataModel
from car_finder.scrappers.scrapper_engine import get_scrapper_engine
from car_finder.utils.amqp_utils import make_basic_pika_consumer, make_basic_pika_publisher
from car_finder.utils.iter_utils import filter_list
from car_finder.utils.log_utils import get_logger
from car_finder.utils.model_utils import model_to_dict
from car_finder.workers.worker import Worker
from pika.adapters.blocking_connection import BlockingChannel
from pika.spec import Basic, BasicProperties

const = get_const()

BROKER_URL = const.AMQP_URL
STAGE = const.STAGE
WORKER_QUEUE = const.SCRAPPER_QUEUE
WORKER_EXCHANGE = const.SCRAPPER_EXCHANGE
UPDATER_QUEUE = const.UPDATER_QUEUE
UPDATER_EXCHANGE = const.UPDATER_EXCHANGE
UPDATER_ROUTING_KEY = const.UPDATER_ROUTING_KEY
logger = get_logger(__name__)

WORKER_QUEUE = f'{WORKER_QUEUE}_{STAGE}'
WORKER_EXCHANGE = f'{WORKER_EXCHANGE}_{STAGE}'

UPDATER_QUEUE = f'{UPDATER_QUEUE}_{STAGE}'
UPDATER_EXCHANGE = f'{UPDATER_EXCHANGE}_{STAGE}'
UPDATER_ROUTING_KEY = f'{UPDATER_ROUTING_KEY}_{STAGE}'


def on_message_callback(ch: BlockingChannel, method: Basic.Deliver, properties: BasicProperties, body):


    execution_dao = ExecutionDAO()

    data = json.loads(body)
    execution_id = data.get("execution_id")

    logger.info(f"Processing Execution: {execution_id}")

    try:
        ch.basic_ack(delivery_tag=method.delivery_tag)
        logger.info(f"Acknowledge Message: '{method.delivery_tag}' for execution '{execution_id}'")

        dao = VehicleTableDAO()
        execution_dao.set_execution_saving_context(execution_id=execution_id)
        execution_data = execution_dao.get_execution(execution_id=execution_id)
        items = execution_data.context
        logger.info(f"Saving {len(items)} records")
        for model in items:
            _model = VehicleDataModel(**model)
            dao.save_record_from_data_model(model=_model)
            logger.info(f"Saved: {_model.vin} {_model.make} {_model.model} {_model.trim}")

        execution_dao.set_execution_complete(execution_id=execution_id)
        logger.info(f"Execution: '{execution_id}' Successful")
        execution_dao.client.close()
        dao.client.close()

    except Exception as err:
        execution_dao.set_execution_failed(execution_id=execution_id)
        execution_dao.set_execution_context(
            execution_id=execution_id,
            context_data={
                "error": str(err),
                "worker": str(__name__)
            }
        )
        logger.info(f"Execution: '{execution_id}' Failed: {err}")
        raise


class UpdaterWorker(Worker):

    def __init__(self):
        self._consumer = make_basic_pika_consumer(
            amqp_url=BROKER_URL,
            queue=UPDATER_QUEUE,
            on_message_callback=on_message_callback
        )

    def run(self, *args, **kwargs):
        self._consumer.run()
