import json
import signal

from car_finder.configuration.constants import get_const
from car_finder.data_access.execution_dao import ExecutionDAO
from car_finder.data_access.vehicle_table_dao import VehicleTableDAO
from car_finder.scrappers.scrapper_engine import get_scrapper_engine
from car_finder.utils.amqp_utils import make_basic_pika_consumer, make_basic_pika_publisher
from car_finder.utils.log_utils import get_logger
from car_finder.utils.model_utils import model_to_dict
from car_finder.workers.worker import Worker
from pika.adapters.blocking_connection import BlockingChannel
from pika.spec import Basic, BasicProperties

const = get_const()

BROKER_URL = const.AMQP_URL
STAGE = const.STAGE
WORKER_QUEUE = const.SCRAPPER_QUEUE
WORKER_EXCHANGE = const.SCRAPPER_EXCHANGE

_FILTER_WORKER_QUEUE = const.FILTER_WORKER_QUEUE
_FILTER_WORKER_EXCHANGE = const.FILTER_WORKER_EXCHANGE
_FILTER_WORKER_ROUTING_KEY = const.FILTER_WORKER_ROUTING_KEY

logger = get_logger(__name__)

WORKER_QUEUE = f'{WORKER_QUEUE}_{STAGE}'
WORKER_EXCHANGE = f'{WORKER_EXCHANGE}_{STAGE}'

FILTER_QUEUE = f'{_FILTER_WORKER_QUEUE}_{STAGE}'
FILTER_EXCHANGE = f'{_FILTER_WORKER_EXCHANGE}_{STAGE}'
FILTER_ROUTING_KEY = f'{_FILTER_WORKER_ROUTING_KEY}_{STAGE}'


def on_message_callback(ch: BlockingChannel, method: Basic.Deliver, properties: BasicProperties, body):
    def process_task(args, source, execution_id, execution_dao, vehicle_table_dao):

        dao = vehicle_table_dao

        try:

            results = get_scrapper_engine().run(scrapper_source=source, **args)
            logger.info(f"Scrapped {len(results)}, {args}")

            execution_dao.set_execution_context(
                execution_id=execution_id,
                context_data=[model_to_dict(model) for model in results]
            )

        except Exception as err:
            logger.error(f'Error: "{err}" while handling message: {execution_id}')
            raise

        try:

            logger.info(f'Execution: {execution_id} context Updated')
            execution_dao.set_execution_pending(execution_id=execution_id)
            publisher = make_basic_pika_publisher(
                amqp_url=BROKER_URL,
                queue=FILTER_QUEUE,
                exchange=FILTER_EXCHANGE,
                routing_key=FILTER_ROUTING_KEY
            )
            publisher.publish_message(body={"action": "update", "execution_id": execution_id})
        except Exception as err:
            logger.error(f"Error while updating execution: {execution_id} context. Error Encountered: {str(err)}")
            raise

    ch.basic_ack(delivery_tag=method.delivery_tag)

    execution_dao = ExecutionDAO()

    def _term_handler(sigid, frame):
        logger.error(f"Received {sigid} - Failing Execution: {execution_id}")
        execution_dao.set_execution_failed(execution_id=execution_id)

    signal.signal(signal.SIGTERM, _term_handler)
    signal.signal(signal.SIGILL, _term_handler)

    data = json.loads(body)
    params = data.get("request").get("params")
    source = data.get("request").get("source")
    execution_id = data.get("_id")
    logger.info(f"Processing Execution: {execution_id}")



    try:

        vehicle_table_dao = VehicleTableDAO()

        execution_dao.set_execution_status_scrapping(execution_id=execution_id)
        logger.info(f"Acknowledged Message: '{method.delivery_tag}' for execution '{execution_id}'")
        process_task(args=params, source=source, execution_id=execution_id, execution_dao=execution_dao,
                     vehicle_table_dao=vehicle_table_dao)
        vehicle_table_dao.client.close()
        execution_dao.client.close()
        logger.info(f"Execution: '{execution_id}' Successfully Parsed")


    except ConnectionResetError as err:
        execution_dao.set_execution_failed(execution_id=execution_id)
        execution_dao.set_execution_context(
            execution_id=execution_id,
            context_data={
                "error": str(err),
                "worker": str(__name__)
            }
        )
        logger.info(f"Execution: '{execution_id}' Failed: {err}")
        raise

    except Exception as err:
        execution_dao.set_execution_failed(execution_id=execution_id)
        execution_dao.set_execution_context(
            execution_id=execution_id,
            context_data={
                "error": str(err),
                "worker": str(__name__)
            }
        )
        logger.info(f"Execution: '{execution_id}' Failed: {err}")
        raise


class ScrapperWorker(Worker):

    def __init__(self):
        self._consumer = make_basic_pika_consumer(
            amqp_url=BROKER_URL,
            queue=WORKER_QUEUE,
            on_message_callback=on_message_callback
        )

    def run(self, *args, **kwargs):
        self._consumer.run()
