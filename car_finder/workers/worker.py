from abc import ABC, abstractmethod


class Worker(ABC):

    @abstractmethod
    def run(self, *args, **kwargs):
        raise NotImplemented
