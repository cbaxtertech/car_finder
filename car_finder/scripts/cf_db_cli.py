import json
import re
from statistics import mean, StatisticsError

import click
from car_finder.data_access.vehicle_table_dao import VehicleTableDAO

dao_map = {
    "truecar": {
        "dao": VehicleTableDAO
    }
}

sources = [
    "truecar"
]


def _get_dao(source):
    return dao_map.get(source)['dao']()


def _extract_queries(query_dict):
    return query_dict['queries']


@click.command()
@click.option('-source', required=True, help='Table Source', type=click.Choice(sources, case_sensitive=False))
@click.option('-query', help='Build Query using JSON file.')
def command_db_cli(source, query):
    if query and source:
        with open(query, "r") as f:
            query_dict = json.load(f)
            queries = query_dict['queries']
            dao = _get_dao(source)

            for query_dict in queries:
                print(query_dict)
                year_range = query_dict.get("year_range", "")
                _query = dao.build_query(query_dict)

                records = dao.query(_query)
                if "-" in year_range:
                    years = year_range.split("-")

                    start, end = years
                    years = list(range(int(start), int(end) + 1))
                    years = [str(year) for year in years]


                else:
                    years = [year_range]

                if len(years) == 1:
                    print(years)
                    year = years.pop()
                    clean_price = calculate_avg_price_clean(records, year)
                    accident_price = calculate_avg_price_w_accidents(records, year)

                    if clean_price:
                        print(f" {year} w/o accidents Average:", clean_price)
                    if accident_price:
                        print(f" {year} w accidents Average:", accident_price)
                else:
                    for year in years:
                        # print(records)
                        clean_price = calculate_avg_price_clean(records, year)
                        accident_price = calculate_avg_price_w_accidents(records, year)

                        if clean_price:
                            print(f" {year} w/o accidents Average:", clean_price)
                        if accident_price:
                            print(f" {year} w accidents Average:", accident_price)

                print("")


def calculate_avg_price_clean(records, year):
    def filter_cars_wo_accidents(record):
        pattern = re.compile(r"No accidents")

        results = pattern.findall(record.car_condition)
        if results:
            return True

    records_wo_accidents = filter(filter_cars_wo_accidents, records)
    car_prices = [
        record.price_num
        for record in records_wo_accidents if record.year == str(year) and record.price_num > 0 and record.millage < 30000
    ]

    try:
        return mean(car_prices)
    except StatisticsError:
        pass
        # print(f" No Cars Matched Criteria {year}")


def calculate_avg_price_w_accidents(records, year):
    records_w_accidents = filter(filter_cars_w_accidents, records)
    car_prices = [
        record.price_num
        for record in records_w_accidents if record.year == str(year) and record.price_num > 0 and record.millage < 30000
    ]
    try:
        return mean(car_prices)
    except StatisticsError:
        pass
        # print(f" No Cars with Accidents Detected {year}")


def filter_cars_w_accidents(record):
    # print(record)
    pattern = re.compile(r"[1-9] accidents")
    results = pattern.findall(record.car_condition)
    if results:
        return True


if __name__ == "__main__":
    command_db_cli()
