import click


@click.command()
@click.option('-make', required=True, help='Make of Car.')
@click.option('-model', required=True, help='Model of Car')
@click.option('-year_range', required=True, help='Year Range, Example: "2018-2019" or "2019"')
@click.option('-save', is_flag=True, default=False, help='Flag to save the results to the database')
@click.option('-print_results', is_flag=True, default=False, help='Displays the output for results')
def build_docker_command(make, model, year_range, save, print_results):
    """
    Builds a Docker command that can be used with pre-built container.

    """
    base_cmd = "docker run --rm -d --name "
    container_image = "cbaxter1988/car_finder"
    container_name = f"tc_{make}_{model}_{year_range}"
    service_cmd = "cf-truecar-cli-scrapper"
    cmd_args = f'-make {make} -model {model} -year_range {year_range}'
    if save:
        cmd_args += ' -save'

    if print_results:
        cmd_args += ' -print_results'

    cmd = base_cmd + f"{container_name} {container_image} {service_cmd} {cmd_args}"

    print("Run following command after pulling container running")
    print(" docker pull cbaxter/1988:car_finder\n")
    print("Docker Command: ")
    print(cmd)


if __name__ == "__main__":
    build_docker_command()
