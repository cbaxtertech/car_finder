import requests

host = '192.168.1.5'


def make_request_body(make, model, start_year, range_=5):
    return {
        "make": make,
        "model": model,
        "start_year": start_year,
        "range": range_
    }


car_models_full = {
    'bmw': [
        'm3',
        'm4',
        'm5',
        'm6',
        'x5',
        'x6',
        'x7',
        '1-series',
        '1-series',
        '3-series',
        '5-series',
        '6-series',
        '7-series'
    ],
    'bentley': [
        "bentayga",
        "continental",
        "continental-gt"
    ],
    'cadillac': [
        'cts',
        'xts',
        'escalade',
        'xlr',
        'elr',
        'ats-v',
    ],
    'chevrolet': [
        'corvette',
        'silverado-1500',
        'trailblazer',
        'camaro',
    ],
    'lexus': [
        'lc',
        'is',
        'es',
        'rx',
        'gs'
    ],
    'ford': [
        'f-150',
        'mustang',
        'bronco-sport',
        'explorer'
    ],
    'ferrari': [
        "430",
        "488",
        "812-superfast",
        "california",
        "california-t"
    ],
    'mercedes-benz': [
        'gle',
        'amg-gt',
        'e-class',
        's-class',
        'c-class',
    ],
    "porsche": [
        'cayenne',
        '911',
        'panamera'
    ],
    "lamborghini": [
        'huracan',
        'murcielago',
        'urus',
        'aventador'
    ],
    'toyota': [
        'camry',
        '86',
        'corolla',
        'highlander',
        'gr-supra'
    ],
    "honda": [
        "civic",
        "accord",
        "ridgeline"
    ],
    "lincoln": [
        "aviator"
    ],
    "tesla": [
        "model-3",
        "model-s",
        "model-x",
        "model-y",
    ],
    "nissan": [
        'gt-r',
        'rouge',
        'pathfinder',
        'maxima',
        'titan',
        'altima'
    ]
}


def main():
    app_requests = []

    request_body = make_request_body(
        make="bmw",
        model='5-series',
        start_year="2020",
        range_=10,
    )
    for make, models in car_models_full.items():
        for model in models:
            app_requests.append(
                make_request_body(
                    make=make,
                    model=model,
                    start_year="2020",
                    range_=10
                )
            )

    for r in app_requests:
        print("")
        response = requests.post(f"http://{host}:5001/api/v1/car_stats/depreciation", json=r)
        data = response.json()['data']
        if data.get("message"):
            pass
        else:
            print(f"Report for '{r['start_year']}-{r['make']}-{r['model']}'")
            print(f"Start Year Average Price: {data['start_average_price']}")
            for result in data['report']:
                print(
                    f"For {result['year']} '{result['average_price']}', there is an {result['percent_change']} change in price ")


if __name__ == "__main__":
    main()
