import click
from car_finder.data_access.vehicle_table_dao import VehicleTableDAO
from car_finder.utils.log_utils import get_global_logger
from car_finder.utils.model_utils import model_to_dict
from car_finder.scrappers.truecar_scrapper import TruecarScrapper
import logging

logger = get_global_logger()


@click.command()
@click.option('-make', required=True, help='Make of Car.')
@click.option('-model', required=True, help='Model of Car')
@click.option('-year_range', required=True, help='Year Range, Example: "2018-2019" or "2019"')
@click.option('-save', is_flag=True, default=False, help='Flag to save the results to the database')
@click.option('-trim', help='Trim Name of Car, Example: "m550i"')
@click.option('-verbose', is_flag=True, default=False, help='Displays the output for results')
@click.option('-print_results', is_flag=True, default=False, help='Displays the output for results')
def command_scrape_truecar(make, model, year_range, trim, save, verbose, print_results):
    logger.debug(f"Command Line Args: (make={make}, model={model}, year_range={year_range}, save={save})")

    if verbose:
        logger.setLevel(logging.DEBUG)
        logger.handlers[0].setLevel(logging.DEBUG)

    tc_query_helper = TruecarScrapper()
    query_results = tc_query_helper.scrape_car(
        make=make,
        model=model,
        year_range=year_range,
        trim=trim
    )

    logger.info(f"Successfully Scrapped '{len(query_results)}' {make} {model} records")

    if save:
        cf_dao = VehicleTableDAO()
        logger.info(f"Attempting to save '{len(query_results)}' records")
        for model in query_results:
            cf_dao.save_record_from_data_model(model=model)

    if print_results:
        print("Printing Results:")
        data_model_dicts = [model_to_dict(model=car_model) for car_model in query_results]
        for car in data_model_dicts:
            print(f' {car}')


if __name__ == '__main__':
    command_scrape_truecar()
