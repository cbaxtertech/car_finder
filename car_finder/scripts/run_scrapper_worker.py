import time
from argparse import ArgumentParser

from car_finder.utils.log_utils import get_logger
from car_finder.workers.scrapper_worker import ScrapperWorker
from pika.exceptions import StreamLostError, ChannelClosedByBroker

logger = get_logger(__name__)


def main():
    parser = ArgumentParser()

    parser.add_argument('--retry_limit', default=5)

    args = parser.parse_args()

    retry_limit = int(args.retry_limit)
    try:
        logger.info("Initiating ScrapperWorker")
        sw = ScrapperWorker()
        sw.run()
    except (StreamLostError, ChannelClosedByBroker) as err:
        print(f"Error Encountered {err}, Retrying in {retry_limit} Seconds")
        time.sleep(retry_limit)
        main()


if __name__ == "__main__":
    main()
