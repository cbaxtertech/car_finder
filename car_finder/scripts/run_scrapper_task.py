import time
from argparse import ArgumentParser
from threading import Thread

from car_finder.utils.http_utils import post_request, get_request
from car_finder.utils.log_utils import get_logger

parser = ArgumentParser()
parser.add_argument('--stage', default='beta')

logger = get_logger("ScrapperTaskRequestScript")

car_models_full = {
    'bmw': [
        'm3',
        'm4',
        'm5',
        'm6',
        'x5',
        'x6',
        'x7',
        '1-series',
        '3-series',
        '5-series',
        '6-series',
        '7-series'
    ],
    'bentley': [
        "bentayga",
        "continental",
        "continental-gt"
    ],
    'cadillac': [
        'cts',
        'xts',
        'escalade',
        'xlr',
        'elr',
        'ats-v',
    ],
    'chevrolet': [
        'corvette',
        'silverado-1500',
        'trailblazer',
        'camaro',
    ],
    'lexus': [
        'lc',
        'is',
        'es',
        'rx',
        'gs'
    ],
    'ford': [
        'f-150',
        'mustang',
        'bronco-sport',
        'explorer'
    ],
    'ferrari': [
        "430",
        "488",
        "812-superfast",
        "california",
        "california-t"
    ],
    'mercedes-benz': [
        'gle',
        'amg-gt',
        'e-class',
        's-class',
        'c-class',
    ],
    "porsche": [
        'cayenne',
        '911',
        'panamera'
    ],
    "lamborghini": [
        'huracan',
        'murcielago',
        'urus',
        'aventador'
    ],
    'toyota': [
        'camry',
        '86',
        'corolla',
        'highlander',
        'gr-supra'
    ],
    "honda": [
        "civic",
        "accord",
        "ridgeline"
    ],
    "lincoln": [
        "aviator"
    ],
    "tesla": [
        "model-3",
        "model-s",
        "model-x",
        "model-y",
    ],
    "nissan": [
        'gt-r',
        'rouge',
        'pathfinder',
        'maxima',
        'titan',
        'altima'
    ]
}

car_models_sample = {
    'bentley': [
        "bentayga",
        "continental",
        "continental-gt"
    ],

    "tesla": [
        "model-3",
        "model-s",
        "model-x",
        "model-y",
    ],
    "porsche": [
        'panamera'
    ],
    "lamborghini": [
        'huracan',
        'murcielago',
        'urus',
        'aventador'
    ],
}

car_models_2 = {
    'bentley': [
        "bentayga",
    ],
    "lamborghini": [
        'huracan',
    ],
}
stage = None
# app_host = "127.0.0.1"
app_host = "192.168.1.5"
app_port = "5001"
# if stage == 'gamma':
#     print("Running Task for Gamma")
#     app_port = "5002"
# else:
#     app_port = '5001'


def handler(url, body):
    response = post_request(url=url, body=body)
    execution_id = response.json().get("data").get("execution_id")
    print(f"Request Sent for {execution_id} - {body}")
    get_response_data = get_request(url=f'http://{app_host}:{app_port}/api/v1/execution/{execution_id}').json().get(
        "data")
    execution_status = get_response_data.get("status")
    execution_request = get_response_data.get("request")

    execution_complete = False
    retry_limit = 50
    retry_count = 1
    while not execution_complete:
        if retry_count == retry_limit:
            print(f"Retry limit exceeded, Possible Issue with Execution {execution_id}")
            return
        if execution_status == "COMPLETED":
            print(
                f"\nExecution '{execution_id} - {body}':  Completed, ScrappedItems='{len(get_response_data.get('context'))}'")
            execution_complete = True
            return

        if execution_status == 'FAILED':
            print(f"\nExecution: {execution_id} Failed to Execute")
            return

        else:
            if execution_status != 'IN_QUEUE':
                print(
                    f"\nExecution: {execution_id} - {body} in {execution_status} state, Attempt: {retry_count}, retrying in {2 * retry_count} Seconds")
            time.sleep(2 * retry_count)
            get_response_data = get_request(
                url=f'http://{app_host}:{app_port}/api/v1/execution/{execution_id}').json().get(
                "data")

            execution_status = get_response_data.get("status")
            retry_count += 1


def main():
    threads = []
    args = parser.parse_args()
    _stage = args.stage
    global stage
    stage = _stage

    for make, models in car_models_full.items():
        for model in models:
            data = {}
            data['make'] = make
            data['model'] = model
            data['year_range'] = '2015-2021'

            args = (
                f"http://{app_host}:{app_port}/api/v1/scrapper/truecar",
                data
            )

            t = Thread(target=handler, args=args, name=f'{data["year_range"]}_{data["make"]}_{data["model"]}',
                       daemon=True)
            logger.info(f"Created Thread TID: {t.name}")
            threads.append(t)

    for t in threads:
        logger.info(f"Starting Thread {t.name}")
        t.start()

    for t in threads:
        logger.info(f"Joining Thread {t.name}")
        t.join()


if __name__ == '__main__':
    main()
