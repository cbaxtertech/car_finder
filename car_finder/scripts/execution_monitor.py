from argparse import ArgumentParser
from os import system, name

from car_finder.data_access.execution_dao import ExecutionDAO
from car_finder.utils.log_utils import get_logger

parser = ArgumentParser()
parser.add_argument('--refresh_interval', default=10)

logger = get_logger("ExecutionMonitor")


def clear():
    logger.info("Clearing Screen")
    # for windows
    if name == 'nt':
        _ = system('cls')

    # for mac and linux(here, os.name is 'posix')
    else:
        _ = system('clear')


def main():
    args = parser.parse_args()
    refresh_interval = args.refresh_interval

    dao = ExecutionDAO()
    import time

    iteration = 0
    while True:
        queued_executions = dao.list_executions_queued()
        scrapping_executions = dao.list_executions_scrapping()
        failed_executions = dao.list_executions_failed()
        pending_executions = dao.list_executions_pending()
        filtering_executions = dao.list_executions_filtering()
        saving_context_executions = dao.list_executions_saving_context()

        iteration += 1
        logger.info(f"Polling {iteration}")
        logger.info(f"Executions in Queue:  {len(queued_executions)}")
        logger.info(f"Executions Scrapping: {len(scrapping_executions)}")
        for e in scrapping_executions:
            logger.info(f'{e._id} - {e.status} - {e.request}')

        logger.info(f"Executions Filtering: {len(filtering_executions)}")
        for e in filtering_executions:
            logger.info(f'{e._id} - {e.status} - {e.request}')

        logger.info(f"Executions Pending Updates: {len(pending_executions)}")
        for e in pending_executions:
            logger.info(f'{e._id} - {e.status} - {e.request}')

        logger.info(f"Executions Saving Actively Saving Context: {len(saving_context_executions)}")
        for e in saving_context_executions:
            logger.info(f'{e._id} - {e.status} - {e.request}')

        logger.info(f"Executions Failed: {len(failed_executions)}")
        for e in failed_executions:
            logger.info(f'{e._id} - {e.status} - {e.request}')

        time.sleep(refresh_interval)


if __name__ == "__main__":
    main()
