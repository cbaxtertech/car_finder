from car_finder.api import app
from car_finder.configuration.constants import get_const

const = get_const()

api_listen_address = const.API_BIND_ADDRESS
api_listen_port = const.API_BIND_PORT
api_debug = const.API_DEBUG



def main():
    # scrapper_engine.run()
    app.run(host=api_listen_address, port=api_listen_port, debug=False)


if __name__ == "__main__":
    main()
