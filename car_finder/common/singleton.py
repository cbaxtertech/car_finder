from car_finder.utils.log_utils import get_logger

logger = get_logger('Singleton')


class Singleton:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = object.__new__(cls, *args, **kwargs)
            return cls._instance
        else:
            logger.warning(f"Only a single instance can be created {cls._instance}")
            return cls._instance


