from car_finder.scrappers.truecar_scrapper import TruecarScrapper


class ScrapperFactory:
    """
    Factory class for creating Scrapper instances.

    """

    @staticmethod
    def get_truecar_scrapper() -> TruecarScrapper:
        return TruecarScrapper()
