class Scrapper:
    def __init__(self, source):
        self.source = source

    def scrape(self, **kwargs):
        raise NotImplemented
