import re
from typing import List

from car_finder.data_access.invalid_vin_table_dao import InvalidVinDAO, InvalidVinModel
from car_finder.metrics.metric_manager import MetricManager
from car_finder.models.data_models.truecar_data_model import VehicleDataModel
from car_finder.models.scrapper_models.truecar_scrapper_model import TruecarScrapperModel
from car_finder.scrappers import Scrapper
from car_finder.utils.bs4_utils import extract_result_set_by_attr, get_parsed_html_page
from car_finder.utils.iter_utils import map_list
from car_finder.utils.log_utils import get_logger

metric_manager = MetricManager()
logger = get_logger('TruecarScrapper')

CAR_LIST_SELECTOR = "#mainContent > div > div.container.container-max-width-3.deprecated-padding-1 > div > div.col-lg-8.col-xl-9 > div > div.margin-bottom-2 > div:nth-child(1) > ul"
PAGINATION_LIST_SELECTOR = "#mainContent > div > div.container.container-max-width-3.deprecated-padding-1 > div > div.col-lg-8.col-xl-9 > div > div.margin-bottom-2 > div.d-flex.justify-content-center > nav > ul"

TRUECAR_BASE_URL = "https://www.truecar.com"
TRUECAR_LISTINGS_URL = f'{TRUECAR_BASE_URL}/used-cars-for-sale/listings/'

ATTRS_SELECTOR_MAP = {
    "prices": {"data-test": "vehicleCardPricingBlockPrice"},
    "year_make_models": {"data-test": "vehicleCardYearMakeModel"},
    "car_colors": {"data-test": "vehicleCardColors"},
    "mileage": {"data-test": "vehicleMileage"},
    "car_links": {"data-test": "vehicleCardLink"},
    "car_locations": {"data-test": "vehicleCardLocation"},
    "car_conditions": {"data-test": "vehicleCardCondition"},
    "car_trims": {"data-test": "vehicleCardTrim"},
    "pagination_selector": {"data-test": "paginationLink"}
}

car_data_tags = [
    "vehicleCardPricingBlockPrice",
    "vehicleCardYearMakeModel",
    "vehicleCardColors",
    "vehicleMileage",
    "vehicleCardLink",
    "vehicleCardLocation",
    "vehicleCardCondition",
    "vehicleCardTrim"
]

year_pattern = re.compile(r"(\d{4})")


def _make_true_car_url_base(
        make,
        model,
        trim=None,
        year_range="2017-2020"
):
    """
    Generates Used car Listing URL for truecar.com


    :param make:
    :param model:
    :param trim: Format Year-Year IE. 2019-2020
    :return:
    """
    url = f'{TRUECAR_LISTINGS_URL}/{make}/{model}/year-{year_range}/?'
    if trim:
        _trim = f'trim={trim}'
        url = url + _trim

    return url


def _make_true_car_url_w_pagination(
        make,
        model,
        page_list,
        trim=None,
        year_range="2017-2020",

):
    urls = []
    for page in page_list:
        url = f'{TRUECAR_BASE_URL}/used-cars-for-sale/listings/{make}/{model}/year-{year_range}/?page={page}'
        if trim:
            _trim = f'&trim={trim}'
            url = url + _trim

        urls.append(url)

    return urls


def _build_truecar_url(vin, year, make, model):
    return f"https://www.truecar.com/used-cars-for-sale/listing/{vin}/{year}-{make}-{model}/"


def _extract_page_number_from_url(url):
    return re.compile("page=(.*)").findall(url)


def _extract_vin_from_tag(tag):
    vin = tag.get_attribute_list('data-test-item')
    return vin[0]


def _extract_car_data_from_tag(tag, make, model, invalid_vin_dao, map_func):
    VALID_DATA_SET_LEN = 9

    vin = _extract_vin_from_tag(tag)
    data_set_tags = extract_result_set_by_attr(tag=tag, attrs={"data-test": car_data_tags})

    data_set_data = [d.text for d in data_set_tags]
    data_set_data.append(vin)

    if len(data_set_data) == VALID_DATA_SET_LEN:
        models = map_list(map_func, [data_set_data])
        logger.debug(
            f"Successfully Scrapped: (vin={vin}, Make='{models[0].make}', model='{models[0].model}', trim='{models[0].trim}')")

        # metric_manager.increment_valid_dataset_metric(make=make, model=model, vin=vin)
        return models
    else:
        logger.error(f"Invalid Data Set for (vin='{vin}', collected={[data.text for data in data_set_tags]})")

        # invalid_vin_dao.save_record(
        #     InvalidVinModel(
        #         make=make,
        #         model=model,
        #         source='truecar.com',
        #         _id=vin,
        #         url=_build_truecar_url(vin=vin, year=year_pattern.findall(data_set_tags[1].text)[0], make=make,
        #                                model=model),
        #         vin=vin
        #     )
        # )
        # metric_manager.increment_invalid_dataset_metric(make=make, model=model, source='truecar')
        return []


class TruecarScrapper(Scrapper):
    def __init__(self, source='truecar.com'):

        super().__init__(source)
        self.car_list_selector = CAR_LIST_SELECTOR
        self.pagination_selector = PAGINATION_LIST_SELECTOR
        self.invalid_vin_dao = InvalidVinDAO()

    def _get_page_list(self, url: str):
        soup = get_parsed_html_page(url)

        pages = soup.select(self.pagination_selector)
        if len(pages) > 0:
            _page = pages.pop()
            page_number_list = _page.find_all(attrs=ATTRS_SELECTOR_MAP.get("pagination_selector"))
            _page_numbers = [int(p.text) for p in page_number_list if p.text.isnumeric()]
            _last_page = _page_numbers[len(_page_numbers) - 1]
            _page_numbers = _page_numbers[:len(_page_numbers) - 1]
            logger.info(f"Detected Pages: {_last_page} for url={url}")
            return _page_numbers, _last_page

        else:
            return [1], None

    def scrape(self, **kwargs) -> List[VehicleDataModel]:
        return self.scrape_car(**kwargs)

    def scrape_car(self, make: str, model: str, year_range: str, trim=None) -> List[VehicleDataModel]:

        def _process_url(_url):
            _page_number = _extract_page_number_from_url(_url)

            logger.info(f"Scrapping URL={_url}")

            def _map_car_model_v2(model_data):
                _price = model_data[3]
                _price_num = int(_price.replace("$", "").replace(",", "")) if isinstance(_price, str) else _price
                vin = model_data[8]

                year = year_pattern.findall(model_data[1])[0]
                millage = re.compile(r"(\d.*).* miles").match(model_data[4])[0].replace(",", "").replace("miles", "")
                millage_int = int(millage)

                return VehicleDataModel(
                    price_num=_price_num,
                    price=_price,
                    color=model_data[6],
                    model=model,
                    make=make,
                    trim=model_data[2],
                    location=model_data[5],
                    year=year,
                    millage=millage_int,
                    car_condition=model_data[7],
                    vin=vin,
                    source="truecar.com",
                    url=_build_truecar_url(vin=vin, make=make, model=model, year=year),
                    year_make_model=f"{year}_{make}_{model}"
                )

            soup = get_parsed_html_page(url=_url)

            car_list_items = soup.select(self.car_list_selector)
            if len(car_list_items) == 1:
                tag = car_list_items.pop()

                car_listing_tags = extract_result_set_by_attr(tag=tag, attrs={
                    "data-test": [
                        "usedListing",

                    ]})
                logger.debug(f'Detected {len(car_listing_tags)} cars on Page {_page_number}')

                data_models = []
                for tag in car_listing_tags:
                    data_model = _extract_car_data_from_tag(
                        tag=tag,
                        model=model,
                        make=make,
                        invalid_vin_dao=self.invalid_vin_dao,
                        map_func=_map_car_model_v2
                    )
                    if data_model:
                        data_models.append(data_model[0])

                return data_models

        base_url = _make_true_car_url_base(
            make=make,
            model=model,
            trim=trim,
            year_range=year_range
        )

        pages, last_page = self._get_page_list(url=base_url)

        if len(pages) > 1:
            urls = _make_true_car_url_w_pagination(
                make=make,
                model=model,
                trim=trim,
                year_range=year_range,
                page_list=[str(i + 1) for i in range(last_page)]
            )

            scrape_results = []
            for url in urls:
                models = _process_url(url)

                if models:
                    scrape_results += models

            return scrape_results

        else:
            return _process_url(base_url)
