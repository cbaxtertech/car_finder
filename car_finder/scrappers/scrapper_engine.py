from typing import List

from car_finder.scrappers.scrapper_factory import ScrapperFactory
from car_finder.models.data_models.vehicle_data_model import VehicleDataModel


class _ScrapperEngine:
    """
    The Scrapper engine executes the appropiate scrapper handler job.

    The engine contains a map of sources to handle
    """

    def __init__(self):
        self._handler_class_map = {
            'truecar': ScrapperFactory.get_truecar_scrapper
        }

    def run(self, scrapper_source, **params) -> List[VehicleDataModel]:
        _scrapper = self._handler_class_map.get(scrapper_source)()
        return _scrapper.scrape(**params)


def get_scrapper_engine():
    return _ScrapperEngine()
