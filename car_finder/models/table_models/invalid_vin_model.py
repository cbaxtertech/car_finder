from dataclasses import dataclass

from car_finder.models.table_models.table_model import TableModel


@dataclass
class InvalidVinModel(TableModel):
    vin: str
    make: str
    model: str
    source: str
    url: str


