from dataclasses import dataclass

from car_finder.models.table_models.table_model import TableModel, _TableModel, MongoTableModel


@dataclass
class VehicleTableModel(_TableModel, MongoTableModel):
    price: str
    price_num: int
    color: str
    millage: int
    year: str
    make: str
    model: str
    trim: str
    source: str
    location: str
    car_condition: str
    year_make_model: str
    url: str
    vin: str = None


def build_vehicle_table_model(
        _id,
        price: str,
        price_num: int,
        color: str,
        millage: int,
        year: str,
        make: str,
        model: str,
        trim: str,
        source: str,
        location: str,
        car_condition: str,
        year_make_model: str,
        url: str,
        vin: str = None
) -> VehicleTableModel:
    return VehicleTableModel(
        price=price,
        price_num=price_num,
        color=color,
        millage=millage,
        year=year,
        make=make,
        model=model,
        trim=trim,
        source=source,
        location=location,
        car_condition=car_condition,
        year_make_model=year_make_model,
        url=url,
        vin=vin,
        _id=_id
    )
