from dataclasses import dataclass
from bson import ObjectId
@dataclass
class _TableModel:
    pass

@dataclass
class TableModel(_TableModel):
    _id: str


@dataclass
class MongoTableModel:
    _id: ObjectId