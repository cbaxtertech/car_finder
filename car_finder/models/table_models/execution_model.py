import dataclasses
from enum import Enum
from typing import Any
from uuid import uuid4

from car_finder.models.table_models.table_model import TableModel


class ExecutionStatues(Enum):
    IN_QUEUE = 'IN_QUEUE'
    PENDING_FILTERING = 'PENDING_FILTERING'
    PENDING_UPDATE = 'PENDING_UPDATE'
    IN_PROGRESS = 'IN_PROGRESS'
    STARTED = 'STARTED'
    COMPLETED = 'COMPLETED'
    CREATED = 'CREATED'
    FAILED = 'FAILED'
    SAVING_CONTEXT = 'SAVING_CONTEXT'
    FILTERING = 'FILTERING'
    SCRAPPING = 'SCRAPPING'


@dataclasses.dataclass
class ExecutionModel(TableModel):
    status: str
    request: Any
    context: Any = None
    created: float =None
    utc_start_time: float = None
    utc_end_time: float = None


def build_execution_model(request, _id=None, status='CREATED', context=None, utc_start_time=None, utc_end_time=None, created=None):
    return ExecutionModel(
        _id=_id or str(uuid4()),
        status=ExecutionStatues(status).value,
        request=request,
        utc_start_time=utc_start_time,
        utc_end_time=utc_end_time,
        context=context,
        created=created
    )
