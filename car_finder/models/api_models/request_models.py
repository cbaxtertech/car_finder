import dataclasses


@dataclasses.dataclass
class ScrapperRequest:
    year_range: str
    make: str
    model: str
    trim: str = ""

