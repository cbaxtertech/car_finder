from dataclasses import dataclass

from car_finder.models.data_models.vehicle_data_model import VehicleDataModel

@dataclass
class TruecarScrapperModel(VehicleDataModel):
    """TrueCar DataModel"""


