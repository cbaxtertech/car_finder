import dataclasses
from uuid import uuid4


@dataclasses.dataclass
class ScrapperRequestMessage:
    execution_id: str
    source: str
    params: dict


@dataclasses.dataclass
class ScrapperTask:
    _id: str
    body: ScrapperRequestMessage
    method: str


def build_message(source: str, params: dict, execution_id: str) -> ScrapperRequestMessage:
    return ScrapperRequestMessage(
        source=source,
        params=params,
        execution_id=execution_id
    )
