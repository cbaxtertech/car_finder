from dataclasses import dataclass
from typing import Any


@dataclass
class PaginationPage:
    cursor: Any
    next_page: int
    current_page: int
    item_count: int

    def __iter__(self):
        return self.cursor


@dataclass
class NewPage:
    cursor: Any
    last_id: str
    total_items: int

    def __iter__(self):
        return self.cursor
