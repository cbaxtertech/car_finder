from dataclasses import dataclass

from car_finder.models.model import Model


@dataclass
class VehicleDataModel(Model):
    price: str
    price_num: int
    color: str
    millage: int
    year: str
    make: str
    model: str
    trim: str
    source: str
    vin: str
    url: str
    year_make_model: str
    location: str
    car_condition: str
