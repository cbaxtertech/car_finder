from setuptools import setup, find_packages

setup(
    name='car_finder',
    version='1',
    url='http://www.github.com/cbaxter1988',
    description='Service that allows users to collect live car market data.',
    author='Courtney S Baxter Jr',
    author_email='cbaxtertech@gmail.com',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'truecar-scrapper=car_finder.scripts.scrapper_scripts.truecar_cli_scraper:command_scrape_truecar',
            'cf-api=car_finder.scripts.run_server:main',
            'cf-scrapper-worker=car_finder.scripts.run_scrapper_worker:main',
            'cf-updater-worker=car_finder.scripts.run_updater_worker:main',
            'cf-filter-worker=car_finder.scripts.run_filter_worker:main',
            'cf-execution-monitor=car_finder.scripts.execution_monitor:main',
            'cf-scrapper-task=car_finder.scripts.run_scrapper_task:main',
        ],
    },
    include_package_data=True
)
