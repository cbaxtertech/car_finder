# Overview

# Requirements 
- Must be able to scrape data from truecars.com

- Must Calculate the average price per make and model W Accidents
- Must Calculate the average price per make and model W/o Accidents
- Must Calculate the average price per make, model, trim w Accidents
- Must Calculate the average price per make, model, trim w/o Accidents
- Must Calculate the average price per make, model, trim, color w Accidents
- Must Calculate the average price per make, model, trim, color w/o Accidents
