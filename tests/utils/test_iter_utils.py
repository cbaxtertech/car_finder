from car_finder.utils.iter_utils import map_list, filter_list, reduce_items


def _map_function(data):
    return tuple(data)


def _filter_function(data):
    if isinstance(data, str):
        return True
    else:
        return False


def test_map_list():
    # Set Up
    len_expectation = 4
    instance_expectation = list

    # Invocation
    actual = map_list(_map_function, ["1", "2", "3", "4"])

    # Validation
    assert len(actual) == len_expectation
    assert isinstance(actual, instance_expectation)


def test_filter_list():
    # Set Up
    len_expectation = 2
    instance_expectation = list

    # Invocation
    actual = filter_list(_filter_function, ["1", "2", (3), (4)])

    # Validation
    assert len(actual) == len_expectation
    assert isinstance(actual, instance_expectation)


def test_reduce_items():
    data = [
        {
            "_id": "60ebc362ce950209725d06c1",
            "price": "$83,999",
            "price_num": 83999,
            "color": "Black exterior, Black interior",
            "millage": 6633,
            "year": "2021",
            "make": "bmw",
            "model": "7-series",
            "trim": "740i RWD",
            "source": "truecar.com",
            "vin": "WBA7T2C05MCF32636",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/WBA7T2C05MCF32636/2021-bmw-7-series/",
            "year_make_model": "2021_bmw_7-series",
            "location": "D'Iberville, MS",
            "car_condition": "No accidents, Personal use"
        },
        {
            "_id": "60ebc362ce950209725d06c1",
            "price": "$83,999",
            "price_num": 83999,
            "color": "Black exterior, Black interior",
            "millage": 6633,
            "year": "2021",
            "make": "bmw",
            "model": "5-series",
            "trim": "740i RWD",
            "source": "truecar.com",
            "vin": "WBA7T2C05MCF32636",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/WBA7T2C05MCF32636/2021-bmw-7-series/",
            "year_make_model": "2021_bmw_7-series",
            "location": "D'Iberville, MS",
            "car_condition": "No accidents, Personal use"
        },
        {
            "_id": "60ebc362ce950209725d06c1",
            "price": "$83,999",
            "price_num": 83999,
            "color": "Black exterior, Black interior",
            "millage": 6633,
            "year": "2021",
            "make": "bmw",
            "model": "3-series",
            "trim": "740i RWD",
            "source": "truecar.com",
            "vin": "WBA7T2C05MCF32636",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/WBA7T2C05MCF32636/2021-bmw-7-series/",
            "year_make_model": "2021_bmw_7-series",
            "location": "D'Iberville, MS",
            "car_condition": "No accidents, Personal use"
        }
    ]

    expectation = {
        '7-series': [
            {'_id': '60ebc362ce950209725d06c1',
             'price': '$83,999',
             'price_num': 83999,
             'color': 'Black exterior, Black interior',
             'millage': 6633,
             'year': '2021',
             'make': 'bmw',
             'model': '7-series',
             'trim': '740i RWD',
             'source': 'truecar.com',
             'vin': 'WBA7T2C05MCF32636',
             'url': 'https://www.truecar.com/used-cars-for-sale/listing/WBA7T2C05MCF32636/2021-bmw-7-series/',
             'year_make_model':
                 '2021_bmw_7-series',
             'location': "D'Iberville, MS",
             'car_condition': 'No accidents, Personal use'}
        ],
        '5-series': [
            {'_id': '60ebc362ce950209725d06c1',
             'price': '$83,999',
             'price_num': 83999,
             'color': 'Black exterior, Black interior',
             'millage': 6633,
             'year': '2021',
             'make': 'bmw',
             'model': '5-series',
             'trim': '740i RWD',
             'source': 'truecar.com',
             'vin': 'WBA7T2C05MCF32636',
             'url': 'https://www.truecar.com/used-cars-for-sale/listing/WBA7T2C05MCF32636/2021-bmw-7-series/',
             'year_make_model': '2021_bmw_7-series', 'location': "D'Iberville, MS",
             'car_condition': 'No accidents, Personal use'}],
        '3-series': [
            {
                '_id': '60ebc362ce950209725d06c1',
                'price': '$83,999', 'price_num': 83999,
                'color': 'Black exterior, Black interior',
                'millage': 6633,
                'year': '2021',
                'make': 'bmw',
                'model': '3-series',
                'trim': '740i RWD',
                'source': 'truecar.com',
                'vin': 'WBA7T2C05MCF32636',
                'url': 'https://www.truecar.com/used-cars-for-sale/listing/WBA7T2C05MCF32636/2021-bmw-7-series/',
                'year_make_model':
                    '2021_bmw_7-series',
                'location': "D'Iberville, MS",
                'car_condition': 'No accidents, Personal use'}
        ]
    }

    actual = reduce_items(field='model', items=data)

    assert actual == expectation
