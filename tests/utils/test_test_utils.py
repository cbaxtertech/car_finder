import pytest
from car_finder.utils.test_utils import (
    assert_is_instance,
    assert_value_equals,
    assert_len_eq,
    assert_is_subclass
)


def test_assert_is_instance():
    with pytest.raises(AssertionError):
        assert_is_instance(testable=['1'], expected_instance=str)


def test_assert_value_equals():
    with pytest.raises(AssertionError):
        assert_value_equals(value1="1", value2="2")


def test_assert_len_eq():
    with pytest.raises(AssertionError):
        assert_len_eq(testable=[1, 2], expected_len=1)


def test_assert_is_subclass():
    with pytest.raises(AssertionError):
        assert_is_subclass(testable=[1, 2], expected_class=dict)
