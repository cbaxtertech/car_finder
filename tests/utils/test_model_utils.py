from dataclasses import dataclass

import pytest
from car_finder.utils.model_utils import model_to_dict, model_to_json, list_fields


@pytest.fixture
def test_model():
    @dataclass
    class TestModel:
        data: dict
        status: bool

    return TestModel(data={"test": "test_payload"}, status=True)


def test_model_to_dict(test_model):
    instance_expectation = dict

    testable = test_model

    actual = model_to_dict(testable)

    assert isinstance(actual, instance_expectation)


def test_model_to_json(test_model):
    instance_expectation = str

    testable = test_model

    actual = model_to_json(testable)

    assert isinstance(actual, instance_expectation)


def test_list_fields(test_model):
    instance_expectation = list
    len_expectation = 2

    testable = test_model

    actual = list_fields(testable)

    assert isinstance(actual, instance_expectation)
    assert len(actual) == len_expectation
