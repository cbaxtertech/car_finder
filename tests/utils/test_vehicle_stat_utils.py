import pytest
from car_finder.utils.core_utils import clone_item
from car_finder.utils.vehicle_stat_utils import (
    DepreciationData,
    DepreciationReport,
    calculate_model_depreciation_from_year,
    calculate_depreciation_from_year,
    calculate_avg_price,
    AveragePriceReturnVal
)


@pytest.fixture
def year_price_tuples():
    return [
        (2019, 20000),
        (2018, 15000),
        (2017, 10000),
        (2016, 5000)
    ]


@pytest.fixture
def vehicle_models(year_price_tuples, vehicle_data_model):
    models = []
    for year, price in year_price_tuples:
        model = clone_item(vehicle_data_model)
        model.price_num = price
        model.year = year
        models.append(model)

    return models


def test_calculate_avg_price(vehicle_models):
    expectation = AveragePriceReturnVal(price=12500, price_list=[20000, 15000, 10000, 5000])

    testable = calculate_avg_price(car_records=vehicle_models)

    assert expectation == testable


def test_calculate_model_depreciation_from_year(vehicle_models):
    expectation = DepreciationReport(
        start_year=2019,
        start_price=20000,
        data=[
            DepreciationData(year=2018, percent_change='-25.00%', price_diff=-5000, price=15000, percent_int=0),
            DepreciationData(year=2017, percent_change='-50.00%', price_diff=-10000, price=10000, percent_int=0),
            DepreciationData(year=2016, percent_change='-75.00%', price_diff=-15000, price=5000, percent_int=-100)
        ]
    )

    testable = calculate_model_depreciation_from_year(
        start_year=2019,
        models=vehicle_models)

    assert testable == expectation


def test_calculate_depreciation_from_year(year_price_tuples):
    # Set Up
    expectation = DepreciationReport(
        start_year=2019,
        start_price=20000,
        data=[
            DepreciationData(
                year=2018,
                percent_change='-25.00%',
                price_diff=-5000,
                price=15000,
                percent_int=0
            ),
            DepreciationData(
                year=2017,
                percent_change='-50.00%',
                price_diff=-10000,
                price=10000,
                percent_int=0
            ),
            DepreciationData(
                year=2016,
                percent_change='-75.00%',
                price_diff=-15000,
                price=5000,
                percent_int=-100
            )
        ]
    )

    # Invocation
    testable = calculate_depreciation_from_year(
        start_year=2019,
        year_price_tuples=year_price_tuples)

    # Validation
    assert testable == expectation
