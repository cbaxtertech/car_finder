import pytest
from car_finder.utils.flask_api_utils import (
    make_data_response,
    make_response_from_items,
    make_response_from_models,
    make_api_response_from_model,
    Response
)


def test_make_data_response():
    instance_expectation = Response
    content_type_expectation = 'application/json'

    actual = make_data_response(type='json', data={})

    assert content_type_expectation in actual.headers.get("content-type")
    assert isinstance(actual, instance_expectation)


def test_make_response_from_items():
    instance_expectation = Response
    content_type_expectation = 'application/json'

    actual = make_response_from_items(type='json', items=[{}, {}])

    assert content_type_expectation in actual.headers.get("content-type")
    assert isinstance(actual, instance_expectation)


def test_make_response_from_items_2():
    with pytest.raises(TypeError):
        make_response_from_items(type='json', items=object)


def test_make_response_from_models(vehicle_table_model):
    instance_expectation = Response
    content_type_expectation = 'application/json'

    actual = make_response_from_models(type='json', models=[vehicle_table_model])

    assert content_type_expectation in actual.headers.get("content-type")
    assert isinstance(actual, instance_expectation)


def test_make_api_response_from_model(vehicle_table_model):
    instance_expectation = Response
    content_type_expectation = 'application/json'

    actual = make_api_response_from_model(type='json', model=vehicle_table_model)

    assert content_type_expectation in actual.headers.get("content-type")
    assert isinstance(actual, instance_expectation)
