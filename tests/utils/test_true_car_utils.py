from car_finder.utils.test_utils import assert_is_instance, assert_len_eq
from car_finder.utils.truecar_utils import extract_records_w_accidents, extract_records_wo_accidents


def test_extract_records_w_accidents(vehicle_table_models_bmw_x5):
    len_expectation = 2
    instance_expectation = list

    models = vehicle_table_models_bmw_x5

    actual = extract_records_w_accidents(records=models)

    assert_len_eq(testable=actual, expected_len=len_expectation)
    assert_is_instance(testable=actual, expected_instance=instance_expectation)


def test_extract_records_wo_accidents(vehicle_table_models_bmw_x5):
    len_expectation = 22
    instance_expectation = list

    models = vehicle_table_models_bmw_x5

    actual = extract_records_wo_accidents(records=models)

    assert_len_eq(testable=actual, expected_len=len_expectation)
    assert_is_instance(testable=actual, expected_instance=instance_expectation)
