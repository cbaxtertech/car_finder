from car_finder.utils.core_utils import make_tuple, len_eq_one


def test_make_tuple():
    actual = make_tuple("test", "test")

    assert len(actual) == 2


def test_len_eq_one_true():
    expectation = True

    actual = len_eq_one(["test"])

    assert actual is expectation


def test_len_eq_one_false():
    expectation = False

    actual = len_eq_one(["test", "test"])

    assert actual is expectation
