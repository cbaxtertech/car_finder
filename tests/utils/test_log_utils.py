from logging import Logger

from car_finder.utils.log_utils import get_logger, get_global_logger, global_logger


def test_get_logger():
    instance_expectation = Logger

    testable = get_logger("test_logger")

    assert isinstance(testable, instance_expectation)


def test_get_global_logger():
    expectation = global_logger

    testable = get_global_logger()

    assert testable == expectation
