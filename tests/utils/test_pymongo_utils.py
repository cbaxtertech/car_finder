import pytest
from car_finder.utils.pymongo_utils import (
    get_client,
    get_database,
    get_collection,
    query_items,
    add_item,
    update_item,
    get_item,
    delete_item,
    scan_items,
    MongoClient,
    make_objectid,
    Database,
    Collection,
    UpdateResult,
    DeleteResult,
    ObjectId,
    DuplicateKeyError

)
from mongomock.collection import Cursor
from tests.settings import get_const

const = get_const()


def test_make_object_id():
    instance_expectation = ObjectId

    testable = make_objectid()

    assert isinstance(testable, instance_expectation)


def test_get_client(MONGO_MOCK_SERVER):
    instance_expectation = MongoClient

    testable = get_client(db_host=const.TEST_MONGO_HOST, db_port=const.TEST_MONGO_PORT)

    assert isinstance(testable, instance_expectation)


def test_get_client_2(MONGO_MOCK_SERVER):
    instance_expectation = MongoClient
    port_expectation = const.TEST_MONGO_PORT

    testable = get_client(db_host=const.TEST_MONGO_HOST)

    assert testable.PORT == port_expectation
    assert isinstance(testable, instance_expectation)


def test_get_database(MONGO_MOCK_SERVER):
    instance_expectation = Database
    client = get_client(db_host=const.TEST_MONGO_HOST)

    testable = get_database(client=client, db_name=const.TEST_MONGO_DB)

    assert isinstance(testable, instance_expectation)


def test_get_collection(MONGO_MOCK_SERVER):
    instance_expectation = Collection
    client = get_client(db_host=const.TEST_MONGO_HOST)
    db = get_database(client=client, db_name=const.TEST_MONGO_DB)

    testable = get_collection(database=db, collection=const.TEST_MONGO_COLLECTION)

    assert isinstance(testable, instance_expectation)


def test_add_item(prep_db, mock_collection):
    with pytest.raises(DuplicateKeyError):
        add_item(collection=mock_collection, item={"val": "1"}, key_id='val')


def test_query_items(prep_db, mock_collection):
    instance_expectation = Cursor
    len_expectation = 1
    collection = mock_collection

    testable = query_items(collection=collection, query={"val": "1"})

    assert isinstance(testable, instance_expectation)
    assert len(list(testable)) == len_expectation


def test_update_item(prep_db, mock_collection):
    instance_expectation = UpdateResult
    modified_count_expectation = 1
    collection = mock_collection

    testable = update_item(collection=collection, item_id="1", item_key='val', new_values={"test": "test"})

    assert isinstance(testable, instance_expectation)
    assert testable.modified_count == modified_count_expectation


def test_scan_items(prep_db, mock_collection):
    instance_expectation = Cursor
    len_expectation = 2
    testable = scan_items(collection=mock_collection)

    assert len(list(testable)) == len_expectation
    assert isinstance(testable, instance_expectation)


def test_get_item(prep_db, mock_collection):
    instance_expectation = Cursor

    testable = get_item(collection=mock_collection, item_id="1")

    assert isinstance(testable, instance_expectation)


def test_delete_item(prep_db, mock_collection):
    instance_expectation = DeleteResult
    delete_count_expectation = 1

    testable = delete_item(collection=mock_collection, item_id="2", item_key="val")

    assert isinstance(testable, instance_expectation)
    assert testable.deleted_count == delete_count_expectation
