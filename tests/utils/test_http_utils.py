from car_finder.utils.http_utils import get_html_page, post_request
from requests import Response
from tests.settings import get_const

const = get_const()


def test_get_html_page(REQUEST_GET_PATCH):
    instance_expectation = str

    actual = get_html_page(const.TEST_URL)

    assert isinstance(actual, instance_expectation)


def test_post_html_page(REQUEST_POST_PATCH):
    instance_expectation = Response

    actual = post_request(const.TEST_URL, {})

    assert isinstance(actual, instance_expectation)
