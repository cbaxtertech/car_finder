import re

from car_finder.utils.regex_utils import make_pattern


def test_make_pattern():
    instance_expectation = re.Pattern

    actual = make_pattern(pattern=r'.*', flags=re.M)

    assert isinstance(actual, instance_expectation)
