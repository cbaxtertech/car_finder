from pconst import Const

BASIC_ENV = {
    "MONGO_DB_HOST": "test_mongo_host",
    "MONGO_DB_PORT": "5001",
    "MONGO_APP_DB": "test_mongo_db",
    "AMQP_USER": "test_amqp_user",
    "AMQP_PW": "test_amqp_pw",
    "AMQP_HOST": "test_amqp_host",
    "AMQP_PORT": "test_amqp_port",
    "VEHICLE_TABLE_ID": "test_vehicle_table_id",
    "EXECUTION_TABLE_ID": "test_execution_table_id",
    "INVALID_VIN_TABLE_ID": "test_vin_table_id",
    "SCRAPPER_EXCHANGE": "cf_exchange",
    "SCRAPPER_QUEUE": "cf_queue",
    "SCRAPPER_ROUTING_KEY": "scrapper",
    "STAGE": "test"
}


class TestConstants(Const):
    TEST_MONGO_PORT = 27017
    TEST_APP_BIND_PORT = 5001
    TEST_MONGO_HOST = 'mongo.test.com'
    TEST_MONGO_DB = 'test_db'
    TEST_MONGO_COLLECTION = 'test_collection'
    TEST_URL = 'https://test.com'



def get_const():
    return TestConstants()
