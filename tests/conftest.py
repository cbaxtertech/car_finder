import os
from unittest.mock import patch

import mongomock
import pytest
from car_finder.models.data_models.vehicle_data_model import VehicleDataModel
from car_finder.models.table_models.execution_model import build_execution_model
from car_finder.models.table_models.vehicle_table_model import build_vehicle_table_model
from car_finder.utils.pymongo_utils import add_item, get_database, get_collection
from requests import Response
from tests.settings import BASIC_ENV, get_const

const = get_const()


# Patch Fixtures
@pytest.fixture(autouse=True)
def BASIC_ENV_PATCH():
    with patch.dict(os.environ, BASIC_ENV, clear=True):
        yield


@pytest.fixture(autouse=False)
def BASIC_ENV_PATCH_EMPTY():
    with patch.dict(os.environ, {}, clear=True):
        yield


@pytest.fixture(autouse=False)
def REQUEST_GET_PATCH():
    with patch("requests.get") as mock:
        mock_response = Response()
        mock_response.status_code = 200
        mock.return_value = mock_response
        yield


@pytest.fixture(autouse=False)
def REQUEST_POST_PATCH():
    with patch("requests.post") as mock:
        mock_response = Response()
        mock_response.status_code = 201
        mock.return_value = mock_response
        yield


@pytest.fixture
def MONGO_MOCK_SERVER():
    with mongomock.patch("mongo.test.com", 27017):
        yield


@pytest.fixture
def mock_client():
    with mongomock.patch(servers=((const.TEST_MONGO_HOST, const.TEST_MONGO_PORT),)) as mongo_mock:
        client = mongomock.MongoClient(host=const.TEST_MONGO_HOST, port=const.TEST_MONGO_PORT)
        yield client


@pytest.fixture
def mock_database(mock_client):
    client = mock_client
    db = get_database(client=client, db_name=const.TEST_MONGO_DB)
    yield db


@pytest.fixture
def mock_collection(mock_database):
    db = mock_database
    collection = get_collection(database=db, collection=const.TEST_MONGO_COLLECTION)
    yield collection


@pytest.fixture
def prep_db(mock_collection):
    test_data_1 = {
        "val": "1",

    }
    test_data_2 = {
        "val": "2",

    }

    collection = mock_collection
    add_item(collection=collection, item=test_data_1, key_id='val')
    add_item(collection=collection, item=test_data_2, key_id='val')


# Normal Fixtures

@pytest.fixture
def PREPARE_MONGO_MOCK_DB():
    with mongomock.patch(servers=(('mongo.test.com', 27017),)):
        client = mongomock.MongoClient(host="mongo.test.com")
        collection = client["test_db"]["test_collection"]
        collection.insert_one({
            "val": "1"
        })
        yield client


@pytest.fixture
def execution_model_created():
    return build_execution_model(
        status="CREATED",
        request={"data": "mock_request"}
    )


@pytest.fixture
def execution_model_in_queue():
    return build_execution_model(
        status="IN_QUEUE",
        request={"data": "mock_request"}
    )


@pytest.fixture
def execution_model_in_progress():
    return build_execution_model(
        status="IN_PROGRESS",
        request={"data": "mock_request"}
    )


@pytest.fixture
def execution_model_pending():
    return build_execution_model(
        status="SCRAPPING",
        request={"data": "mock_request"}
    )


@pytest.fixture
def execution_model_started():
    return build_execution_model(
        status="STARTED",
        request={"data": "mock_request"}
    )


@pytest.fixture
def execution_model_completed():
    return build_execution_model(
        status="COMPLETED",
        request={"data": "mock_request"}
    )


@pytest.fixture
def execution_model_failed():
    return build_execution_model(
        status="FAILED",
        request={"data": "mock_request"}
    )


@pytest.fixture
def vehicle_data_model():
    return VehicleDataModel(
        year_make_model="2020 bmw x5",
        location="Orlando Fl",
        model="x5",
        car_condition="Clean",
        vin="12345678",
        url="https://test.com",
        millage=20000,
        make="bmw",
        year="2020",
        source="test.com",
        price_num=25000,
        price="25000",
        color="black",
        trim="test_trim",
    )


@pytest.fixture
def vehicle_table_model():
    return build_vehicle_table_model(
        year_make_model="2020 bmw x5",
        location="Orlando Fl",
        model="x5",
        car_condition="Clean",
        vin="12345678",
        url="https://test.com",
        millage=20000,
        make="bmw",
        year="2020",
        source="test.com",
        price_num=25000,
        price="25000",
        color="black",
        trim="test_trim",
        _id='60eaf60bb29edbb736385e45'
    )


@pytest.fixture
def vehicle_table_data_bmw():
    return {

        "price": "$20,995",
        "price_num": 20995,
        "color": "Blue exterior, Black interior",
        "millage": 83817,
        "year": "2009",
        "make": "bmw",
        "model": "m6",
        "trim": "Convertible",
        "source": "truecar.com",
        "location": "Bayonne, NJ",
        "car_condition": "No accidents, 4 Owners, Personal use",
        "year_make_model": "2009_bmw_m6",
        "url": "https://www.truecar.com/used-cars-for-sale/listing/WBSEK93599CY80101/2009-bmw-m6/",
        "vin": "WBSEK93599CY80101"
    }


@pytest.fixture
def vehicle_table_bmw_x5_records_raw():
    return [
        {
            "_id": "5UXKR0C34H0V73109",
            "price": "$33,499",
            "price_num": 33499,
            "color": "Gray exterior, Black interior",
            "millage": 41944,
            "year": "2017",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i AWD",
            "source": "truecar.com",
            "location": "Manheim, PA",
            "car_condition": "No accidents, 1 Owner, Personal use",
            "year_make_model": "2017_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR0C34H0V73109/2017-bmw-x5/",
            "vin": "5UXKR0C34H0V73109"
        },
        {
            "_id": "5UXKR0C36H0V69045",
            "price": "$33,490",
            "price_num": 33490,
            "color": "Gray exterior, Black interior",
            "millage": 47966,
            "year": "2017",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i AWD",
            "source": "truecar.com",
            "location": "Hyattsville, MD",
            "car_condition": "2 accidents, 1 Owner, Personal use",
            "year_make_model": "2017_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR0C36H0V69045/2017-bmw-x5/",
            "vin": "5UXKR0C36H0V69045"
        },
        {
            "_id": "5UXKR0C59J0X91568",
            "price": "$32,999",
            "price_num": 32999,
            "color": "Silver exterior, Black interior",
            "millage": 59241,
            "year": "2018",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i AWD",
            "source": "truecar.com",
            "location": "Maple Shade, NJ",
            "car_condition": "1 accident, 2 Owners, Fleet use",
            "year_make_model": "2018_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR0C59J0X91568/2018-bmw-x5/",
            "vin": "5UXKR0C59J0X91568"
        },
        {
            "_id": "5UXKR0C59JL070147",
            "price": "$26,649",
            "price_num": 26649,
            "color": "Black exterior, Black interior",
            "millage": 110031,
            "year": "2018",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i AWD",
            "source": "truecar.com",
            "location": "Stratham, NH",
            "car_condition": "No accidents, 1 Owner, Personal use",
            "year_make_model": "2018_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR0C59JL070147/2018-bmw-x5/",
            "vin": "5UXKR0C59JL070147"
        },
        {
            "_id": "5UXZV4C53CL759878",
            "price": "$13,995",
            "price_num": 13995,
            "color": "Black exterior, Black interior",
            "millage": 95975,
            "year": "2012",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i Premium AWD",
            "source": "truecar.com",
            "location": "Tampa, FL",
            "car_condition": "No accidents, 3 Owners, Personal use",
            "year_make_model": "2012_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXZV4C53CL759878/2012-bmw-x5/",
            "vin": "5UXZV4C53CL759878"
        },
        {
            "_id": "5UXKR2C59H0U18536",
            "price": "$21,999",
            "price_num": 21999,
            "color": "White exterior, Brown interior",
            "millage": 144318,
            "year": "2017",
            "make": "bmw",
            "model": "x5",
            "trim": "sDrive35i RWD",
            "source": "truecar.com",
            "location": "Houston, TX",
            "car_condition": "No accidents, 1 Owner, Personal use",
            "year_make_model": "2017_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR2C59H0U18536/2017-bmw-x5/",
            "vin": "5UXKR2C59H0U18536"
        },
        {
            "_id": "5UXFE4C56AL383760",
            "price": "$9,500",
            "price_num": 9500,
            "color": "White exterior, Unknown interior",
            "millage": 128125,
            "year": "2010",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive30i AWD",
            "source": "truecar.com",
            "location": "El Cajon, CA",
            "car_condition": "No accidents, 3 Owners, Personal use",
            "year_make_model": "2010_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXFE4C56AL383760/2010-bmw-x5/",
            "vin": "5UXFE4C56AL383760"
        },
        {
            "_id": "5UXZW0C54CL667376",
            "price": "$15,500",
            "price_num": 15500,
            "color": "Red exterior, Brown interior",
            "millage": 77390,
            "year": "2012",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35d",
            "source": "truecar.com",
            "location": "Albuquerque, NM",
            "car_condition": "1 accident, 1 Owner, Personal use",
            "year_make_model": "2012_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXZW0C54CL667376/2012-bmw-x5/",
            "vin": "5UXZW0C54CL667376"
        },
        {
            "_id": "5UXKR0C59F0K52620",
            "price": "$19,995",
            "price_num": 19995,
            "color": "White exterior, Brown interior",
            "millage": 92011,
            "year": "2015",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i AWD",
            "source": "truecar.com",
            "location": "Newark, NJ",
            "car_condition": "No accidents, 3 Owners, Personal use",
            "year_make_model": "2015_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR0C59F0K52620/2015-bmw-x5/",
            "vin": "5UXKR0C59F0K52620"
        },
        {
            "_id": "5UXKR2C51J0Z14193",
            "price": "$35,995",
            "price_num": 35995,
            "color": "White exterior, Black interior",
            "millage": 32016,
            "year": "2018",
            "make": "bmw",
            "model": "x5",
            "trim": "sDrive35i RWD",
            "source": "truecar.com",
            "location": "Inglewood, CA",
            "car_condition": "No accidents, 1 Owner, Personal use",
            "year_make_model": "2018_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR2C51J0Z14193/2018-bmw-x5/",
            "vin": "5UXKR2C51J0Z14193"
        },
        {
            "_id": "5UXZW0C50CL672218",
            "price": "$11,985",
            "price_num": 11985,
            "color": "Gray exterior, Black interior",
            "millage": 125288,
            "year": "2012",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35d",
            "source": "truecar.com",
            "location": "Garden Grove, CA",
            "car_condition": "No accidents, 1 Owner, Personal use",
            "year_make_model": "2012_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXZW0C50CL672218/2012-bmw-x5/",
            "vin": "5UXZW0C50CL672218"
        },
        {
            "_id": "5UXKR6C53J0U14270",
            "price": "$31,399",
            "price_num": 31399,
            "color": "Unknown exterior, Unknown interior",
            "millage": 87101,
            "year": "2018",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive50i AWD",
            "source": "truecar.com",
            "location": "New Smyrna Beach, FL",
            "car_condition": "1 accident, 1 Owner, Fleet use",
            "year_make_model": "2018_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR6C53J0U14270/2018-bmw-x5/",
            "vin": "5UXKR6C53J0U14270"
        },
        {
            "_id": "5UXKS4C59E0J96916",
            "price": "$19,999",
            "price_num": 19999,
            "color": "Black exterior, Unknown interior",
            "millage": 89913,
            "year": "2014",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35d AWD",
            "source": "truecar.com",
            "location": "Seattle, WA",
            "car_condition": "No accidents, 2 Owners, Personal use",
            "year_make_model": "2014_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKS4C59E0J96916/2014-bmw-x5/",
            "vin": "5UXKS4C59E0J96916"
        },
        {
            "_id": "5UXKR6C5XJ0U14489",
            "price": "$43,999",
            "price_num": 43999,
            "color": "White exterior, Black interior",
            "millage": 28740,
            "year": "2018",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive50i AWD",
            "source": "truecar.com",
            "location": "Winter Garden, FL",
            "car_condition": "No accidents, 1 Owner, Personal use",
            "year_make_model": "2018_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR6C5XJ0U14489/2018-bmw-x5/",
            "vin": "5UXKR6C5XJ0U14489"
        },
        {
            "_id": "5UXKR2C56E0C01816",
            "price": "$19,495",
            "price_num": 19495,
            "color": "Black exterior, Black interior",
            "millage": 87274,
            "year": "2014",
            "make": "bmw",
            "model": "x5",
            "trim": "sDrive35i RWD",
            "source": "truecar.com",
            "location": "Chula Vista, CA",
            "car_condition": "No accidents, 1 Owner, Personal use",
            "year_make_model": "2014_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR2C56E0C01816/2014-bmw-x5/",
            "vin": "5UXKR2C56E0C01816"
        },
        {
            "_id": "5UXKR2C58J0X08062",
            "price": "$35,400",
            "price_num": 35400,
            "color": "Black exterior, Black interior",
            "millage": 32740,
            "year": "2018",
            "make": "bmw",
            "model": "x5",
            "trim": "sDrive35i RWD",
            "source": "truecar.com",
            "location": "N. Miami Beach, FL",
            "car_condition": "No accidents, 1 Owner, Personal use",
            "year_make_model": "2018_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR2C58J0X08062/2018-bmw-x5/",
            "vin": "5UXKR2C58J0X08062"
        },
        {
            "_id": "5UXZV4C54D0E02876",
            "price": "$16,995",
            "price_num": 16995,
            "color": "Black exterior, Black interior",
            "millage": 90454,
            "year": "2013",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i Sport Activity AWD",
            "source": "truecar.com",
            "location": "Westport, MA",
            "car_condition": "2 accidents, 2 Owners, Fleet use",
            "year_make_model": "2013_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXZV4C54D0E02876/2013-bmw-x5/",
            "vin": "5UXZV4C54D0E02876"
        },
        {
            "_id": "5UXZV4C5XD0G57080",
            "price": "$16,995",
            "price_num": 16995,
            "color": "Black exterior, Unknown interior",
            "millage": 86192,
            "year": "2013",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i Premium AWD",
            "source": "truecar.com",
            "location": "Westport, MA",
            "car_condition": "No accidents, 1 Owner, Personal use",
            "year_make_model": "2013_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXZV4C5XD0G57080/2013-bmw-x5/",
            "vin": "5UXZV4C5XD0G57080"
        },
        {
            "_id": "5UXZV4C57DL994186",
            "price": "$15,499",
            "price_num": 15499,
            "color": "Blue exterior, Gray interior",
            "millage": 89823,
            "year": "2013",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i Sport Activity AWD",
            "source": "truecar.com",
            "location": "Fremont, CA",
            "car_condition": "No accidents, 2 Owners, Personal use",
            "year_make_model": "2013_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXZV4C57DL994186/2013-bmw-x5/",
            "vin": "5UXZV4C57DL994186"
        },
        {
            "_id": "5UXZV4C53BL404868",
            "price": "$10,499",
            "price_num": 10499,
            "color": "Black exterior, Unknown interior",
            "millage": 114853,
            "year": "2011",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i Premium AWD",
            "source": "truecar.com",
            "location": "Tampa, FL",
            "car_condition": "1 accident, 4 Owners, Fleet use",
            "year_make_model": "2011_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXZV4C53BL404868/2011-bmw-x5/",
            "vin": "5UXZV4C53BL404868"
        },
        {
            "_id": "5UXZV4C5XBL413647",
            "price": "$15,995",
            "price_num": 15995,
            "color": "Brown exterior, Black interior",
            "millage": 46569,
            "year": "2011",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i AWD",
            "source": "truecar.com",
            "location": "Dallas, TX",
            "car_condition": "No accidents, 1 Owner, Personal use",
            "year_make_model": "2011_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXZV4C5XBL413647/2011-bmw-x5/",
            "vin": "5UXZV4C5XBL413647"
        },
        {
            "_id": "5UXZV4C5XD0E06415",
            "price": "$14,000",
            "price_num": 14000,
            "color": "Silver exterior, Black interior",
            "millage": 65392,
            "year": "2013",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i AWD",
            "source": "truecar.com",
            "location": "Grapevine, TX",
            "car_condition": "1 accident, 3 Owners, Fleet use",
            "year_make_model": "2013_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXZV4C5XD0E06415/2013-bmw-x5/",
            "vin": "5UXZV4C5XD0E06415"
        },
        {
            "_id": "5UXKR0C5XF0K54005",
            "price": "$22,999",
            "price_num": 22999,
            "color": "Gold exterior, Unknown interior",
            "millage": 64990,
            "year": "2015",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i AWD",
            "source": "truecar.com",
            "location": "Costa Mesa, CA",
            "car_condition": "No accidents, 2 Owners, Personal use",
            "year_make_model": "2015_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR0C5XF0K54005/2015-bmw-x5/",
            "vin": "5UXKR0C5XF0K54005"
        },
        {
            "_id": "5UXZW0C54BL368579",
            "price": "$11,500",
            "price_num": 11500,
            "color": "Brown exterior, Beige interior",
            "millage": 116191,
            "year": "2011",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35d AWD",
            "source": "truecar.com",
            "location": "Littleton, CO",
            "car_condition": "1 accident, 2 Owners, Personal use",
            "year_make_model": "2011_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXZW0C54BL368579/2011-bmw-x5/",
            "vin": "5UXZW0C54BL368579"
        },
        {
            "_id": "5UXKR2C59E0H31889",
            "price": "$16,989",
            "price_num": 16989,
            "color": "White exterior, Black interior",
            "millage": 126873,
            "year": "2014",
            "make": "bmw",
            "model": "x5",
            "trim": "sDrive35i RWD",
            "source": "truecar.com",
            "location": "North Hills, CA",
            "car_condition": "No accidents, 2 Owners, Personal use",
            "year_make_model": "2014_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR2C59E0H31889/2014-bmw-x5/",
            "vin": "5UXKR2C59E0H31889"
        },
        {
            "_id": "5UXKR0C56J0X85436",
            "price": "$39,687",
            "price_num": 39687,
            "color": "Black exterior, Black interior",
            "millage": 39955,
            "year": "2018",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i AWD",
            "source": "truecar.com",
            "location": "Jacksonville, FL",
            "car_condition": "No accidents, 1 Owner, Personal use",
            "year_make_model": "2018_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR0C56J0X85436/2018-bmw-x5/",
            "vin": "5UXKR0C56J0X85436"
        },
        {
            "_id": "5UXKR0C55H0U52509",
            "price": "$29,240",
            "price_num": 29240,
            "color": "Black exterior, Black interior",
            "millage": 71599,
            "year": "2017",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i AWD",
            "source": "truecar.com",
            "location": "Renton, WA",
            "car_condition": "No accidents, 2 Owners, Personal use",
            "year_make_model": "2017_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKR0C55H0U52509/2017-bmw-x5/",
            "vin": "5UXKR0C55H0U52509"
        },
        {
            "_id": "5UXKT0C52J0W04057",
            "price": "$43,995",
            "price_num": 43995,
            "color": "White exterior, Black interior",
            "millage": 32835,
            "year": "2018",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive40e iPerformance AWD",
            "source": "truecar.com",
            "location": "Seattle, WA",
            "car_condition": "No accidents, 1 Owner, Personal use",
            "year_make_model": "2018_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXKT0C52J0W04057/2018-bmw-x5/",
            "vin": "5UXKT0C52J0W04057"
        },
        {
            "_id": "5UXZV8C5XBL418659",
            "price": "$10,999",
            "price_num": 10999,
            "color": "Gray exterior, Gray interior",
            "millage": 135633,
            "year": "2011",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive50i AWD",
            "source": "truecar.com",
            "location": "Tampa, FL",
            "car_condition": "No accidents, 3 Owners, Fleet use",
            "year_make_model": "2011_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXZV8C5XBL418659/2011-bmw-x5/",
            "vin": "5UXZV8C5XBL418659"
        },
        {
            "_id": "5UXZV4C59D0B04808",
            "price": "$13,495",
            "price_num": 13495,
            "color": "Gray exterior, Black interior",
            "millage": 99601,
            "year": "2013",
            "make": "bmw",
            "model": "x5",
            "trim": "xDrive35i AWD",
            "source": "truecar.com",
            "location": "Raleigh, NC",
            "car_condition": "No accidents, 2 Owners, Fleet use",
            "year_make_model": "2013_bmw_x5",
            "url": "https://www.truecar.com/used-cars-for-sale/listing/5UXZV4C59D0B04808/2013-bmw-x5/",
            "vin": "5UXZV4C59D0B04808"
        }
    ]


@pytest.fixture
def vehicle_table_models_bmw_x5(vehicle_table_bmw_x5_records_raw):
    return [
        build_vehicle_table_model(**data)
        for data in vehicle_table_bmw_x5_records_raw
    ]
