from copy import deepcopy
from unittest.mock import patch

import pytest
from car_finder.models.table_models.vehicle_table_model import VehicleTableModel


@pytest.fixture
@patch("car_finder.utils.pymongo_utils.MongoClient")
def mongodb_dao_patch(mc_patch, mock_client):
    from car_finder.data_access.vehicle_table_dao import VehicleTableDAO
    mc_patch.return_value = mock_client
    dao = VehicleTableDAO()

    return dao


@pytest.fixture
def prepared_vehicle_table_dao(mongodb_dao_patch, vehicle_table_model):
    dao = mongodb_dao_patch
    for i in range(10):
        item = deepcopy(vehicle_table_model)
        item.vin = f'{i + 1}'
        item._id = f'{i + 1}'
        dao.save_record(item)

    return dao


def test_vehicle_table_dao_save_record(mongodb_dao_patch, vehicle_data_model):
    expectation = True
    dao = mongodb_dao_patch
    actual = dao.save_record(data_model=vehicle_data_model)

    assert actual is True


def test_vehicle_table_dao_save_record_2(prepared_vehicle_table_dao, vehicle_data_model):
    expectation = False
    dao = prepared_vehicle_table_dao

    data = deepcopy(vehicle_data_model)

    data.vin = "1"

    actual = dao.save_record(data_model=data)

    assert actual is expectation


def test_vehicle_table_dao_get_record_count(prepared_vehicle_table_dao):
    expectation = 10

    actual = prepared_vehicle_table_dao.get_record_count()

    assert actual == expectation


def test_vehicle_table_dao_get_records(prepared_vehicle_table_dao):
    instance_expectation = VehicleTableModel
    len_expectation = 10

    actual = prepared_vehicle_table_dao.get_records()

    assert isinstance(actual[0], instance_expectation)
    assert len(actual) == len_expectation


def test_vehicle_table_dao_delete_record(prepared_vehicle_table_dao):
    expectation = True

    actual = prepared_vehicle_table_dao.delete_record(record_id="1")

    assert actual is expectation


def test_vehicle_table_dao_delete_record_false(prepared_vehicle_table_dao):
    expectation = False

    actual = prepared_vehicle_table_dao.delete_record(record_id=99)

    assert actual is expectation


def test_vehicle_table_dao_update_record(prepared_vehicle_table_dao, vehicle_data_model):
    expectation = True

    data = deepcopy(vehicle_data_model)

    actual = prepared_vehicle_table_dao.update_record(record_id="1", model=data)

    assert actual is expectation


def test_vehicle_table_dao__process_query(prepared_vehicle_table_dao):
    instance_expectation = list

    actual = prepared_vehicle_table_dao._process_query(query={"bad": "query"})

    assert isinstance(actual, instance_expectation)


def test_vehicle_table_dao_get_records_by_make(prepared_vehicle_table_dao):
    expectation = 10

    actual = prepared_vehicle_table_dao.get_records_by_make(make="bmw")

    assert len(actual) == expectation


def test_vehicle_table_dao_get_record_by_vin(prepared_vehicle_table_dao):
    instance_expectation = VehicleTableModel

    actual = prepared_vehicle_table_dao.get_record_by_vin(vin="1")

    assert isinstance(actual, instance_expectation)


def test_vehicle_table_dao_get_records_by_make_model(prepared_vehicle_table_dao):
    expectation = 10

    actual = prepared_vehicle_table_dao.get_records_by_make_model(make="bmw", model="x5")

    assert len(actual) == expectation


def test_vehicle_table_dao_get_records_by_make_model_trim(prepared_vehicle_table_dao):
    expectation = 10

    actual = prepared_vehicle_table_dao.get_records_by_make_model_trim(make="bmw", model="x5", trim='test_trim')

    assert len(actual) == expectation


def test_vehicle_table_dao_get_records_by_make_model_trim_year(prepared_vehicle_table_dao):
    expectation = 10

    actual = prepared_vehicle_table_dao.get_records_by_make_model_trim_year(make="bmw", model="x5", trim='test_trim',
                                                                            year='2020')

    assert len(actual) == expectation


def test_vehicle_table_dao_list_make_models(prepared_vehicle_table_dao):
    expectation = 1

    actual = prepared_vehicle_table_dao.list_make_models(make="bmw")

    assert len(actual) == expectation


def test_vehicle_table_dao_list_model_trims(prepared_vehicle_table_dao):
    expectation = 1

    actual = prepared_vehicle_table_dao.list_model_trims(make="bmw", model='x5')

    assert len(actual) == expectation


def test_vehicle_table_dao_list_makes(prepared_vehicle_table_dao):
    expectation = 1

    actual = prepared_vehicle_table_dao.list_model_trims(make="bmw", model='x5')

    assert len(actual) == expectation


def test_vehicle_table_dao_list_years_by_make_model(prepared_vehicle_table_dao):
    expectation = 1

    actual = prepared_vehicle_table_dao.list_years_by_make_model(make="bmw", model='x5')

    assert len(actual) == expectation


def test_vehicle_table_dao_list_years_by_make_model_trim(prepared_vehicle_table_dao):
    expectation = 1

    actual = prepared_vehicle_table_dao.list_years_by_make_model_trim(make="bmw", model='x5', trim='test_trim')

    assert len(actual) == expectation


def test_vehicle_table_dao_build_query(prepared_vehicle_table_dao):
    instance_expectation = dict

    actual = prepared_vehicle_table_dao.build_query(query={"make": "bmw"})

    assert isinstance(actual, instance_expectation)


def test_vehicle_table_dao_build_query_2(prepared_vehicle_table_dao):
    instance_expectation = dict
    expectation = {"make": "bmw"}

    actual = prepared_vehicle_table_dao.build_query(query={"make": "bmw", "year_range": "bad_entry"})

    assert actual == expectation
    assert isinstance(actual, instance_expectation)
