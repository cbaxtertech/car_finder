from tests.settings import get_const

const = get_const()
from unittest.mock import patch
from mongomock.collection import Cursor


@patch("car_finder.data_access.mongodb_dao.get_database")
@patch("car_finder.data_access.mongodb_dao.get_collection")
@patch("car_finder.utils.pymongo_utils.MongoClient")
def test_mongodb_dao_scan_items(
        pymongo_utils_client_mock,
        pymongo_utils_get_collection_mock,
        pymongo_utils_get_database_mock,
        BASIC_ENV_PATCH,
        mock_client,
        mock_collection,
        mock_database,
        prep_db):
    # Set Up
    from car_finder.data_access.mongodb_dao import MongoDBDAO
    pymongo_utils_client_mock.return_value = mock_client
    pymongo_utils_get_collection_mock.return_value = mock_collection
    pymongo_utils_get_database_mock.return_value = mock_database

    # Expectations
    instance_expectation = Cursor

    testable = MongoDBDAO(
        host=const.TEST_MONGO_HOST,
        port=const.TEST_MONGO_PORT,
        db=const.TEST_MONGO_DB,
        collection=const.TEST_MONGO_COLLECTION
    )

    actual = testable.scan_items()

    assert isinstance(actual, instance_expectation)
    assert isinstance(actual.next(), dict)


@patch("car_finder.data_access.mongodb_dao.get_database")
@patch("car_finder.data_access.mongodb_dao.get_collection")
@patch("car_finder.utils.pymongo_utils.MongoClient")
def test_mongodb_dao_get_item(
        pymongo_utils_client_mock,
        pymongo_utils_get_collection_mock,
        pymongo_utils_get_database_mock,
        BASIC_ENV_PATCH,
        mock_client,
        mock_collection,
        mock_database,
        prep_db):
    # Set Up
    from car_finder.data_access.mongodb_dao import MongoDBDAO
    pymongo_utils_client_mock.return_value = mock_client
    pymongo_utils_get_collection_mock.return_value = mock_collection
    pymongo_utils_get_database_mock.return_value = mock_database

    # Expectations
    instance_expectation = Cursor

    testable = MongoDBDAO(
        host=const.TEST_MONGO_HOST,
        port=const.TEST_MONGO_PORT,
        db=const.TEST_MONGO_DB,
        collection=const.TEST_MONGO_COLLECTION
    )

    actual = testable.get_item(item_key='val', item_id='1')

    assert isinstance(actual, instance_expectation)
    assert isinstance(actual.next(), dict)


@patch("car_finder.data_access.mongodb_dao.get_database")
@patch("car_finder.data_access.mongodb_dao.get_collection")
@patch("car_finder.utils.pymongo_utils.MongoClient")
def test_mongodb_dao_query_items(
        pymongo_utils_client_mock,
        pymongo_utils_get_collection_mock,
        pymongo_utils_get_database_mock,
        BASIC_ENV_PATCH,
        mock_client,
        mock_collection,
        mock_database,
        prep_db):
    # Set Up
    from car_finder.data_access.mongodb_dao import MongoDBDAO
    pymongo_utils_client_mock.return_value = mock_client
    pymongo_utils_get_collection_mock.return_value = mock_collection
    pymongo_utils_get_database_mock.return_value = mock_database

    # Expectations
    instance_expectation = Cursor

    testable = MongoDBDAO(
        host=const.TEST_MONGO_HOST,
        port=const.TEST_MONGO_PORT,
        db=const.TEST_MONGO_DB,
        collection=const.TEST_MONGO_COLLECTION
    )

    actual = testable.query_items(query={"val": "1"})

    assert isinstance(actual, instance_expectation)
    assert isinstance(actual.next(), dict)


@patch("car_finder.data_access.mongodb_dao.get_database")
@patch("car_finder.data_access.mongodb_dao.get_collection")
@patch("car_finder.utils.pymongo_utils.MongoClient")
def test_mongodb_dao_add_item(
        pymongo_utils_client_mock,
        pymongo_utils_get_collection_mock,
        pymongo_utils_get_database_mock,
        BASIC_ENV_PATCH,
        mock_client,
        mock_collection,
        mock_database,
        prep_db):
    # Set Up
    from car_finder.data_access.mongodb_dao import MongoDBDAO, InsertOneResult
    pymongo_utils_client_mock.return_value = mock_client
    pymongo_utils_get_collection_mock.return_value = mock_collection
    pymongo_utils_get_database_mock.return_value = mock_database

    # Expectations
    instance_expectation = InsertOneResult
    insert_id_expectation = "99"
    testable = MongoDBDAO(
        host=const.TEST_MONGO_HOST,
        port=const.TEST_MONGO_PORT,
        db=const.TEST_MONGO_DB,
        collection=const.TEST_MONGO_COLLECTION
    )

    actual = testable.add_item(item={"val": "99"}, key_id="val")

    assert isinstance(actual, instance_expectation)
    assert actual.inserted_id == insert_id_expectation
    assert actual.acknowledged


@patch("car_finder.data_access.mongodb_dao.get_database")
@patch("car_finder.data_access.mongodb_dao.get_collection")
@patch("car_finder.utils.pymongo_utils.MongoClient")
def test_mongodb_dao_delete_item(
        pymongo_utils_client_mock,
        pymongo_utils_get_collection_mock,
        pymongo_utils_get_database_mock,
        BASIC_ENV_PATCH,
        mock_client,
        mock_collection,
        mock_database,
        prep_db):
    # Set Up
    from car_finder.data_access.mongodb_dao import MongoDBDAO, DeleteResult
    pymongo_utils_client_mock.return_value = mock_client
    pymongo_utils_get_collection_mock.return_value = mock_collection
    pymongo_utils_get_database_mock.return_value = mock_database

    # Expectations
    instance_expectation = DeleteResult
    delete_count_expectation = 1
    testable = MongoDBDAO(
        host=const.TEST_MONGO_HOST,
        port=const.TEST_MONGO_PORT,
        db=const.TEST_MONGO_DB,
        collection=const.TEST_MONGO_COLLECTION
    )

    testable.add_item(item={"val": "99"}, key_id="val")
    actual = testable.delete_item(item_id='99', item_key='val')

    assert isinstance(actual, instance_expectation)
    assert actual.deleted_count == delete_count_expectation
    assert actual.acknowledged


@patch("car_finder.data_access.mongodb_dao.get_database")
@patch("car_finder.data_access.mongodb_dao.get_collection")
@patch("car_finder.utils.pymongo_utils.MongoClient")
def test_mongodb_dao_update_item(
        pymongo_utils_client_mock,
        pymongo_utils_get_collection_mock,
        pymongo_utils_get_database_mock,
        BASIC_ENV_PATCH,
        mock_client,
        mock_collection,
        mock_database,
        prep_db):
    # Set Up
    from car_finder.data_access.mongodb_dao import MongoDBDAO, UpdateResult
    pymongo_utils_client_mock.return_value = mock_client
    pymongo_utils_get_collection_mock.return_value = mock_collection
    pymongo_utils_get_database_mock.return_value = mock_database

    # Expectations
    instance_expectation = UpdateResult
    modified_count_expectation = 1
    testable = MongoDBDAO(
        host=const.TEST_MONGO_HOST,
        port=const.TEST_MONGO_PORT,
        db=const.TEST_MONGO_DB,
        collection=const.TEST_MONGO_COLLECTION
    )

    testable.add_item(item={"val": "99"}, key_id="val")
    actual = testable.update_item(item_id='99', item_key='val', new_values={"test": "test"})

    assert isinstance(actual, instance_expectation)
    assert actual.modified_count == modified_count_expectation
    assert actual.acknowledged



@patch("car_finder.data_access.mongodb_dao.get_database")
@patch("car_finder.data_access.mongodb_dao.get_collection")
@patch("car_finder.utils.pymongo_utils.MongoClient")
def test_mongodb_dao_client(
        pymongo_utils_client_mock,
        pymongo_utils_get_collection_mock,
        pymongo_utils_get_database_mock,
        BASIC_ENV_PATCH,
        mock_client,
        mock_collection,
        mock_database,
        prep_db):
    # Set Up
    from car_finder.data_access.mongodb_dao import MongoDBDAO, UpdateResult
    pymongo_utils_client_mock.return_value = mock_client
    pymongo_utils_get_collection_mock.return_value = mock_collection
    pymongo_utils_get_database_mock.return_value = mock_database

    # Expectations
    instance_expectation = UpdateResult
    modified_count_expectation = 1
    testable = MongoDBDAO(
        host=const.TEST_MONGO_HOST,
        port=const.TEST_MONGO_PORT,
        db=const.TEST_MONGO_DB,
        collection=const.TEST_MONGO_COLLECTION
    )
    actual = testable.client

    assert actual == mock_client


@patch("car_finder.data_access.mongodb_dao.get_database")
@patch("car_finder.data_access.mongodb_dao.get_collection")
@patch("car_finder.utils.pymongo_utils.MongoClient")
def test_mongodb_dao_db(
        pymongo_utils_client_mock,
        pymongo_utils_get_collection_mock,
        pymongo_utils_get_database_mock,
        BASIC_ENV_PATCH,
        mock_client,
        mock_collection,
        mock_database,
        prep_db):
    # Set Up
    from car_finder.data_access.mongodb_dao import MongoDBDAO, UpdateResult
    pymongo_utils_client_mock.return_value = mock_client
    pymongo_utils_get_collection_mock.return_value = mock_collection
    pymongo_utils_get_database_mock.return_value = mock_database

    # Expectations
    instance_expectation = UpdateResult
    modified_count_expectation = 1
    testable = MongoDBDAO(
        host=const.TEST_MONGO_HOST,
        port=const.TEST_MONGO_PORT,
        db=const.TEST_MONGO_DB,
        collection=const.TEST_MONGO_COLLECTION
    )
    actual = testable.db

    assert actual == mock_database