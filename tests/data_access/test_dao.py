def test_dao_mixin_interface(BASIC_ENV_PATCH):
    from car_finder.data_access.dao import DAOMixinInterface

    class TestMixin(DAOMixinInterface):
        def query_items(self, query):
            pass

        def get_item(self, item_id, item_key):
            pass

        def update_item(self, item_id, new_values, item_key):
            pass

        def add_item(self, item: dict):
            pass

        def delete_item(self, item_id, item_key):
            pass

        def scan_items(self):
            pass

    testable = TestMixin()
