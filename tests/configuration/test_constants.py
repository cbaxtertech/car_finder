import pytest
from tests.settings import BASIC_ENV


def test_required_vars_failed(BASIC_ENV_PATCH_EMPTY):
    with pytest.raises(KeyError):
        from car_finder.configuration.constants import get_const
        get_const()


def test_required_vars(BASIC_ENV_PATCH):
    from car_finder.configuration.constants import get_const

    testable = get_const()

    required_environment_vars = [
        'MONGO_DB_HOST',
        'MONGO_DB_PORT',
        'MONGO_APP_DB',
        'EXECUTION_TABLE_ID',
        'VEHICLE_TABLE_ID',
        'INVALID_VIN_TABLE_ID',
        'AMQP_USER',
        'AMQP_PW',
        'AMQP_HOST',
        'AMQP_PORT',
        'SCRAPPER_EXCHANGE',
        'SCRAPPER_QUEUE',
        'SCRAPPER_ROUTING_KEY',
    ]

    for var in required_environment_vars:
        expectation = BASIC_ENV.get(var)
        assert var in dir(testable)
        assert getattr(testable, var) == expectation
