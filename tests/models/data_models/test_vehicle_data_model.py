from car_finder.models.data_models.vehicle_data_model import VehicleDataModel


def test_vehicle_data_model():
    # Set Up
    instance_expectation = VehicleDataModel

    # Invocation

    testable = VehicleDataModel(
        price="test_price",
        price_num=10000,
        color="test_color",
        millage=5000,
        year="test_year",
        make="test_make",
        model="test_model",
        trim="test_trim",
        source="test_source",
        vin="test_vin",
        url="test_url",
        location="test_location",
        car_condition="test_car_condtion",
        year_make_model="test_year_make_model",
    )

    # Validation
    assert isinstance(testable, instance_expectation)
