from car_finder.models.scrapper_models import ScrapperTask, ScrapperRequestMessage, build_message


def test_scrapper_message():
    # Set Up
    instance_expectation = ScrapperRequestMessage

    # Invocation

    actual = build_message(source="test_source", params={}, execution_id="test_id")

    # Validation

    assert isinstance(actual, instance_expectation)


def test_scrapper_task():
    # Set Up
    instance_expectation = ScrapperTask

    # Invocation
    actual = ScrapperTask(
        _id="test_id",
        body=build_message(source="test_source", params={}, execution_id="test_id"),
        method="test_method"
    )

    # Validation

    assert isinstance(actual, instance_expectation)
