from car_finder.models.api_models.request_models import ScrapperRequest


def test_scrapper_request_1():
    # Set Up
    instance_expectation = ScrapperRequest

    # Invocation
    testable = ScrapperRequest(year_range="1999-2000", make="test_make", model="test_model", trim="test_trim")

    # Validation
    assert isinstance(testable, instance_expectation)
