from car_finder.models.table_models.invalid_vin_model import InvalidVinModel


def test_basic_invalid_vin_table_model():
    instance_expectation = InvalidVinModel

    actual = InvalidVinModel(_id="12345", vin="1234", make="bmw", source="test.com", url="http://test.com", model="x5")

    assert isinstance(actual, instance_expectation)
