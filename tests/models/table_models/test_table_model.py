from car_finder.models.table_models.table_model import TableModel


def test_basic_table_model():
    # set up
    instance_expectation = TableModel
    id_expectation = "123456789"

    # Invocation
    actual = TableModel(_id="123456789")

    # Validation
    assert isinstance(actual, instance_expectation)
    assert actual._id == id_expectation
