import pytest
from car_finder.models.table_models.execution_model import ExecutionStatues, ExecutionModel


@pytest.mark.parametrize(
    ["test_input", "expected"],
    [
        ("IN_QUEUE", "IN_QUEUE"),
        ("SCRAPPING", "SCRAPPING"),
        ("IN_PROGRESS", "IN_PROGRESS"),
        ("STARTED", "STARTED"),
        ("COMPLETED", "COMPLETED"),
        ("CREATED", "CREATED"),
        ("FAILED", "FAILED"),
    ]
)
def test_execution_statuses(test_input, expected):
    assert ExecutionStatues(test_input).value == expected


def test_execution_model_created(execution_model_created):
    # Set Up
    instance_expectation = ExecutionModel
    status_expectation = "CREATED"

    # Invocation
    execution_model = execution_model_created

    # Verification
    assert execution_model.status == status_expectation
    assert isinstance(execution_model, instance_expectation)


def test_execution_model_in_queue(execution_model_in_queue):
    # Set Up
    instance_expectation = ExecutionModel
    status_expectation = "IN_QUEUE"

    # Invocation
    execution_model = execution_model_in_queue

    # Verification
    assert execution_model.status == status_expectation
    assert isinstance(execution_model, instance_expectation)


def test_execution_model_in_progress(execution_model_in_progress):
    # Set Up
    instance_expectation = ExecutionModel
    status_expectation = "IN_PROGRESS"

    # Invocation
    execution_model = execution_model_in_progress

    # Verification
    assert execution_model.status == status_expectation
    assert isinstance(execution_model, instance_expectation)


def test_execution_model_pending(execution_model_pending):
    # Set Up
    instance_expectation = ExecutionModel
    status_expectation = "SCRAPPING"

    # Invocation
    execution_model = execution_model_pending

    # Verification
    assert execution_model.status == status_expectation
    assert isinstance(execution_model, instance_expectation)


def test_execution_model_started(execution_model_started):
    # Set Up
    instance_expectation = ExecutionModel
    status_expectation = "STARTED"

    # Invocation
    execution_model = execution_model_started

    # Verification
    assert execution_model.status == status_expectation
    assert isinstance(execution_model, instance_expectation)


def test_execution_model_completed(execution_model_completed):
    # Set Up
    instance_expectation = ExecutionModel
    status_expectation = "COMPLETED"

    # Invocation
    execution_model = execution_model_completed

    # Verification
    assert execution_model.status == status_expectation
    assert isinstance(execution_model, instance_expectation)


def test_execution_model_failed(execution_model_failed):
    # Set Up
    instance_expectation = ExecutionModel
    status_expectation = "FAILED"

    # Invocation
    execution_model = execution_model_failed

    # Verification
    assert execution_model.status == status_expectation
    assert isinstance(execution_model, instance_expectation)