from car_finder.models.table_models.vehicle_table_model import VehicleTableModel


def test_basic_vehicle_table_model(vehicle_table_model):
    # Set up
    instance_expectation = VehicleTableModel

    # Invocation
    actual = vehicle_table_model

    # Validation
    assert isinstance(actual, instance_expectation)


def test_vehicle_table_model_property_types(vehicle_table_model):
    # Set up
    millage_type_expectation = int
    price_num_type_expectation = int

    # Invocation
    actual = vehicle_table_model

    # Validation
    assert isinstance(actual.millage, millage_type_expectation)
    assert isinstance(actual.price_num, price_num_type_expectation)